<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = ['user_phone', 'user_code', 'response', 'request_time'];
}
