<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Game extends Model
{
    //

    protected $fillable = [
        'user_phone',
        'user_code',
        'fullname',
        'birthday',
        'email',
        'user_code',
        'gift_id',
        'gift_type',
        'ip_address',
        'gender'
    ];

	protected $giftNames = [
		7 => 'giải đặc biệt',
		1 => 'giải nhất',
		2 => 'giải nhì',
		3 => 'giải ba',
		4 => 'giải tư',
		5 => 'giải năm',
		6 => 'giải khuyến khích'
	];

    protected $aGiftTitle = [
        7 => '05 chỉ vàng',
        1 => '10 triệu điểm ExtraCare',
        2 => '02 triệu điểm ExtraCare',
        3 => '01 triệu điểm ExtraCare',
        4 => '500 nghìn điểm ExtraCare',
        5 => '200 nghìn điểm ExtraCare',
        6 => '20 nghìn điểm ExtraCare',
    ];

	public function gift(){
		return $this->belongsTo(Gift::class);
	}

	public function getPhoneMaskedAttribute(){
		return substr($this->user_phone, 0, 4) . 'xxx' . substr($this->user_phone, -3, 3);
	}

	public function getGiftTitleAttribute(){
		return isset($this->giftNames[$this->gift_type]) ? $this->giftNames[$this->gift_type] : 'n/a';
	}

	public function getGiftNameAttribute(){
        return isset($this->aGiftTitle[$this->gift_type]) ? $this->aGiftTitle[$this->gift_type] : 'n/a';
    }

	static function todayGiftCount(){
		return self::whereRaw("DATE(created_at) = '" . Carbon::now()->toDateString() . "'")
			->where('gift_type', '>', 0)
			->groupBy(DB::raw('DATE(created_at)'))
			->count();
	}
}
