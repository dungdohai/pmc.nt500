<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Gift extends Model
{
	use SoftDeletes;

	protected $casts = [
		'valid_from' => 'date',
		'valid_to' => 'date'
	];

	var $aGiftKey = [
        7 => 's_gift_quantity',
        1 => 'first_gift_quantity',
        2 => 'second_gift_quantity',
        3 => 'third_gift_quantity',
        4 => 'fourth_gift_quantity',
        5 => 'fifth_gift_quantity',
        6 => 'kk_gift_quantity'
    ];

	var $aGiftCountKey = [
        7 => 's_gift_count',
        1 => 'first_gift_count',
        2 => 'second_gift_count',
        3 => 'third_gift_count',
        4 => 'fourth_gift_count',
        5 => 'fifth_gift_count',
        6 => 'kk_gift_count',
    ];

	public static function current(){
	    $currentDate = Carbon::now()->toDateString();
	    return self::where([
	       ['valid_from', '<=', $currentDate],
	       ['valid_to', '>=', $currentDate],
        ])->first();
    }

    private function _countGift($aGiftKey){
        $totalGift = 0;
        foreach ($aGiftKey as $key){
            $totalGift += $this->$key;
        }

        return $totalGift;
    }

    private function _checkUserWin($userPhone){
        $isWin = Game::where('user_phone', $userPhone)
            ->where('gift_type', '>', 0)
            ->count();

        return $isWin > 0;
    }

    private function _countGameToday(){
	    return Game::whereDate('created_at', Carbon::now()->toDateString())->count();
    }

    private function _isWin(){
	    $valid_from = $this->valid_from;
	    $valid_to = $this->valid_to->addDays(1);

        $diff = $valid_from->diffInSeconds($valid_to);
        $diff_current = Carbon::now()->diffInSeconds($valid_to);

        $totalGift = $this->_countGift($this->aGiftKey);
        $remainGift = $totalGift - $this->_countGift($this->aGiftCountKey);

        if ($totalGift == 0 || $remainGift <= 0){
            return false;
        }

        $rate = floor($diff/$totalGift);
        $rate_current = floor($diff_current/$remainGift);

        $rand = rand(0, $rate);

        return $rand >= $rate_current;
    }

    public function luckydraw($userPhone){
        $giftType = 0;

        if ($this->_checkUserWin($userPhone)){
            return $giftType;
        }

        if ($this->_countGameToday() < 0){
            return $giftType;
        }

	    if ($this->_isWin()){
            $aRandomKey = [];

            foreach ($this->aGiftKey as $id => $totalKey){
                $countKey = $this->aGiftCountKey[$id];
                $giftRemain = $this->$totalKey - $this->$countKey;

                if ($giftRemain>0){
                    $aRandomKey = array_merge($aRandomKey, array_fill(0, $giftRemain, $id));
                }
            }

            if (sizeof($aRandomKey) > 0){
                $giftTypeKey = array_rand($aRandomKey);
                $giftType = $aRandomKey[$giftTypeKey];

                $this->increment($this->aGiftCountKey[$giftType], 1);
            }
        }

        return $giftType;
    }
}
