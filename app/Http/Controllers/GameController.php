<?php

namespace App\Http\Controllers;

use App\Codes;
use App\Game;
use App\Gift;
use App\Services\Lib;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    //

    public function checkCode(Request $request){
        $phone = $request->get('phone');
        $code = $request->get('code');
        $fullname = $request->get('fullname');
        $birthday = $request->get('birthday');
        $gender = $request->get('gender', 0);
        $email = $request->get('email');

        Cookie::queue('customer', json_encode(compact([
            'phone',
            'code',
            'email',
            'fullname',
            'birthday',
            'gender',
        ])), 30*24*60);

        if (empty($phone) || !in_array(strlen($phone), [10, 11]) || !is_numeric($phone)){
            return Lib::response(false, 'Số điện thoại không hợp lệ');
        }

        if (empty($code) || empty($fullname) || empty($birthday)){
            return Lib::response(false, 'Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)){
            return Lib::response(false, 'Địa chỉ email không hợp lệ.');
        }

        $dbCode = Codes::check($phone, $code);

        if (empty($dbCode)){
            return Lib::response(false, 'Mã dự thưởng không hợp lệ');
        }

        if ($dbCode->remain <= 0){
            return Lib::response(false, 'Mã dự thưởng đã hết lượt bốc thăm');
        }

        return Lib::response(true, 'success', $dbCode);
    }

    private function _checkUserWin($userPhone){
        $isWin = Game::where('user_phone', $userPhone)
            ->where('gift_type', '>', 0)
            ->count();

        return $isWin > 0;
    }

    public function lucky(Request $request){
        $response = $this->checkCode($request);
        if (!$response['status']){
            return $response;
        }

        DB::beginTransaction();
        $code = Codes::lockForUpdate()->find($response['data']->id);
        $code->decrement('remain');
        DB::commit();

        $game = Game::create([
            'user_phone' => $request->phone,
            'user_code' => $code->code,
            'fullname' => $request->fullname,
            'birthday' => $request->birthday,
            'email' => $request->email,
            'gender' => $request->get('gender', 0),
            'ip_address' => $request->ip()
        ]);

        // GET gift
        $gift = Gift::current();

        $aGiftTitle = [
            7 => '05 chỉ vàng',
            1 => '10 triệu điểm ExtraCare',
            2 => '02 triệu điểm ExtraCare',
            3 => '01 triệu điểm ExtraCare',
            4 => '500 nghìn điểm ExtraCare',
            5 => '200 nghìn điểm ExtraCare',
            6 => '20 nghìn điểm ExtraCare',
        ];

        if (empty($gift)){
            return Lib::response(true, 'Chúc bạn may mắn lần sau!', ['code' => $code, 'game' => $game]);
        }

        $game->gift_id = $gift->id;
        $game->gift_type = $gift->luckydraw($request->phone);
        $game->save();

        $message = isset($aGiftTitle[$game->gift_type]) ? sprintf('Chúc mừng bạn đã trúng thưởng<br/><strong>%s</strong>', $aGiftTitle[$game->gift_type]) : 'Chúc bạn may mắn lần sau!';

        $message .= sprintf('<br/>Bạn còn <strong>%d lượt bốc thăm</strong>', $code->remain);

        return Lib::response(true, $message, [
            'code' => $code,
            'game' => $game
        ]);
    }
}
