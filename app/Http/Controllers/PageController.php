<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Codes;
use App\Game;
use App\Gift;
use App\Log;
use App\Product;
use App\Promotion;
use App\Section;
use App\Setting;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class PageController extends Controller
{
    public function index(){
    	$config = Setting::first();
    	$sections = Section::where('status', 1)->orderBy('weight')->get();

    	$promotions = Promotion::where('status', 1)->orderBy('weight')->get();

    	$games = Game::with('gift')
            ->where('gift_type', '>', 0)
		    ->orderBy('created_at', 'desc');

    	$gameCount = $games->count();
    	$games = $games->limit(50)->get();

    	$products = Product::orderBy('created_at', 'desc')->get();
    	$blogs = Blog::orderBy('created_at', 'desc')->get();

    	return View('pages/home-official', [
    		'config' => $config,
		    'sections' => $sections,
		    'promotions' => $promotions,
		    'games' => $games,
		    'gameCount' => $gameCount,
		    'products' => $products,
		    'blogs' => $blogs,
            'customer' => json_decode(Cookie::get('customer', '[]'))
	    ]);
    }


    public function thele(Request $request){
	    $config = Setting::first();

	    $sections = Section::where('status', 1)->orderBy('weight')->get();

	    $games = Game::where('gift_type', '>', 0)
//		    ->where('gift_id', $gift->id)
//		    ->orderBy('gift_type', 'asc')
		    ->orderBy('created_at', 'desc')
		    ->get();

	    return View('pages/thele', [
		    'config' => $config,
		    'games' => $games,
		    'sections' => $sections
	    ]);
    }

    public function apiSearch(Request $request){
    	$phone = $request->get('phone');

    	if (empty($phone)){
    		return [];
	    }

    	$game = Game::with('gift')->where('user_phone', $phone)->where('gift_type', '>', 0)->first();

    	if ($game){
    	    return [[
    	        'user_phone' => $game->user_phone,
                'created_at' => $game->created_at->toDateTimeString(),
                'gift_title' => $game->gift->title . ' - ' . $game->gift_title,
                'gift_name' => $game->gift_name
            ]];
        }

    	return [];
    }

    public function check(Request $request){
    	$codes = $this->_getUserCodes($request->get('phone'), $request->get('code', 'API_CHECK'));
    	$games = Game::where('user_phone', $request->get('phone'))->get();
    	$codes['game'] = [];

    	foreach ($games as $game){
    		$codes['game'][$game->user_code] = [
    			'date' => $game->created_at->toDateTimeString(),
			    'gift' => $game->gift_type
		    ];
	    }
    	dd($codes);
    }

    private function _getUserCodes($phone, $code = ''){
    	$url = config('app.api_url');

    	$params = [
    		"query" => 'mutation getEventCustomerProfile($phone:String!,$campaign_id:Int!){getEventCustomerProfile(phone:$phone,campaign_id:$campaign_id){codes{code,times}}}',
		    "variables" => [
		    	"phone" => $phone,
			    "campaign_id" => (int) config('app.api_campaign_id')
		    ]
	    ];

    	$requestTime = microtime(true);
    	$headers = [
    		'pharmacity-partner-api-token' => 'asWSoeWJbCZlk6I8ttBKyUq5lChIhP1m'
	    ];

    	$client = new Client();
    	$response = $client->request(
    		'POST',
		    $url,
		    [
		    	"headers" => $headers,
		    	"form_params" => $params,
			    "http_errors" => false
		    ]
	    );

    	$logs = Log::create([
    		'user_phone' => $phone,
		    'user_code' => $code,
		    'response' => $response->getBody()->getContents(),
		    'request_time' => (microtime(true) - $requestTime)
	    ]);

    	if ($response->getStatusCode() === 200){
		    $datas = json_decode($logs->response);

		    if ($datas){
		    	if (isset($datas->errors)){
		    		$error = array_pop($datas->errors);
		    		return [
		    			'status' => false,
					    'data' => $error->message
				    ];
			    }else{
				    $datas = $datas->data->getEventCustomerProfile->codes;

				    $codes = [];
				    foreach ($datas as $data){
					    $codes[$data->code] = $data->times;
				    }

				    return [
				    	'status' => true,
					    'data' => $codes
				    ];
			    }
		    }
	    }

    	return [
    		'status' => false,
		    'data' => 'Lỗi kết nối đến hệ thống quay thưởng'
	    ];
    }

    private function _validateUserCode($phone, $code){
    	$response = $this->_getUserCodes($phone, $code);

    	if ($response['status']){
		    return [
		    	'status' => in_array($code, $response['data']),
			    'response' => in_array($code, $response['data']) ? 'Hãy chọn mở một trái tim để có cơ hội nhận quà nhé!' : 'Mã dự thưởng không hợp lệ'
		    ];
	    }

    	return [
    		'status' => false,
		    'response' => $response['data']
	    ];
    }

    private function _checkCodeExist($userPhone, $userCode){
	    if ($userCode == 'FBLIKECODE'){
		    return [
			    'code' => 1,
			    'message' => 'Hãy chọn mở một lọ thuốc để có cơ hội nhận quà nhé!'
		    ];
	    }

	    $response = $this->_validateUserCode($userPhone, $userCode);

	    if ($response['status']){
	    	return [
	    		'code' => 1,
			    'message' => 'Hãy chọn mở một lọ thuốc để có cơ hội nhận quà nhé!'
		    ];
	    }

	    return [
		    'code' => -1,
		    'message' => $response['response']
	    ];
    }

    private function _checkUserWin($userPhone){
    	$isWin = Game::where('user_phone', $userPhone)
		    ->where('gift_type', '>', 0)
		    ->count();

    	return $isWin > 0;
    }

    public function api(Request $request){
    	$type = $request->get('type', 'check');

	    $userPhone = $request->get('user_phone');
	    $userCode = $request->get('user_code');

	    $userCode = strtoupper($userCode);

	    if (strpos($userCode, 'FB') === 0){
		    $userCode = 'FBLIKECODE';
	    }

	    $code = $this->_checkUserCode($userPhone, $userCode);

	    if (!$code){

        }

//	    $codeExists = Game::where([
//		    'user_phone' => $userPhone,
//		    'user_code' => $userCode
//	    ])->count();
//
//	    if ($codeExists > 0){
//		    return [
//			    'code' => -1,
//			    'message' => 'Mã thẻ đã được sử dụng'
//		    ];
//	    }

    	if ($type == 'check'){
    		return $this->_checkCodeExist($userPhone, $userCode);
	    }

    	if ($type == 'code'){
		    $game = new Game();
		    $game->user_phone = $userPhone;
		    $game->user_code = $userCode;
		    $game->ip_address = $request->ip();
		    $game->save();

    		$response = $this->_checkCodeExist($userPhone, $userCode);

    		if ($response['code'] == 1){
			    $gift = $this->_getGiftConfig();
			    $giftId = $gift ? $gift->id : 0;

			    if ($this->_checkUserWin($userPhone)){
				    $giftType = 0;
			    }else{
				    $giftType = $gift ? $gift->getRandomGift($userCode) : 0;
			    }

			    $game->gift_id = $giftId;
			    $game->gift_type = $giftType;
			    $game->save();

			    $aGift = [
				    1 => 'ĐẶC BIỆT<br/><b>iPhone XS MAX 256Gb</b>',
				    2 => 'NHẤT<br/><b>Một chỉ vàng SJC</b>',
				    3 => 'NHÌ<br/><b>Một chai viên uống bổ sung Vitamin C Blackmores BIO C 1000mg</b>',
				    4 => 'BA<br/><b>Tích luỹ 20.000đ vào thẻ Extracare</b>'
			    ];

			    if ($giftType > 0){
				    return [
					    'code' => 1,
					    'message' => 'Xin chúc mừng!<br/>Bạn đã trúng GIẢI ' . $aGift[$giftType]
				    ];
			    }
		    }

		    return [
			    'code' => 1,
			    'message' => 'Chúc bạn may mắn lần sau!'
		    ];
	    }
    }

    function _checkUserCode($userPhone, $userCode){
        $code = Codes::where([
            'phone' => $userPhone,
            'code' => $userCode
        ])->first();

        $response = false;

        if (empty($code)){
            $response = $this->_getUserCodes($userPhone, $userCode);
            if ($response['status']){
                foreach ($response['data'] as $code => $times){
                    $newCode = Codes::firstOrCreate([
                        'phone' => $userPhone,
                        'code' => $userCode
                    ], [
                        'count' => $times,
                        'remain' => $times
                    ]);

                    if ($code == $userCode){
                        $response = $newCode;
                    }
                }
            }
        }

        return $response;
    }

    private function _getGiftConfig(){
    	$date = Carbon::now()->toDateString();

    	$gift = Gift::where('valid_from', '<=', $date)
		    ->where('valid_to', '>=', $date)->first();

    	return $gift;
    }

    private function _response($status=true, $message=''){
        return [
            'code' => $status ? 1 : -1,
            'message' => $message
        ];
    }
}
