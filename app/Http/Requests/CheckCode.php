<?php

namespace App\Http\Requests;

use App\Codes;
use Illuminate\Foundation\Http\FormRequest;

class CheckCode extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|size:10',
            'code' => [
                'required',
                function ($attribute, $value) {
                    $codes = Codes::check($this->phone, $value);

                    if (empty($code)){
                        return false;
                    }
                }
            ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required' => 'Vui lòng nhập vào số điện thoại của bạn',
            'phone.length' => 'Số điện thoại không hợp lệ',
            'code.required' => 'Vui lòng nhập vào mã dự thưởng của bạn',
        ];
    }
}
