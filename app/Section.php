<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['menu', 'title', 'teaser', 'content', 'weight', 'status'];
}
