<?php

namespace App;

use App\Services\PmcApi;
use Illuminate\Database\Eloquent\Model;

class Codes extends Model
{
    //

    protected $fillable = [
        'phone',
        'code',
        'count',
        'remain'
    ];

    static function check($phone, $code){
        $code = strtoupper($code);

        $responseCode = self::where([
            'phone' => $phone,
            'code' => $code
        ])->first();

        if (empty($responseCode)){
            $api = new PmcApi();
            $apiCodes = $api->getCode($phone);
            foreach ($apiCodes as $apiCode){
                $dbCode = self::firstOrCreate(
                    [
                        'phone' => $phone,
                        'code' => $apiCode->code
                    ],
                    [
                        'count' => $apiCode->times,
                        'remain' => $apiCode->times,
                    ]
                );

                if ($apiCode->code === $code){
                    $responseCode = $dbCode;
                }
            }
        }

        return $responseCode;
    }
}
