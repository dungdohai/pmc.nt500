<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Infinety\Filemanager\FilemanagerField;
use Johnathan\NovaTrumbowyg\NovaTrumbowyg;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;


class Setting extends Resource
{
	/**
	 * The model the resource corresponds to.
	 *
	 * @var  string
	 */
	public static $model = 'App\Setting';

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var  string
	 */
	public static $title = 'id';

	/**
	 * The columns that should be searched.
	 *
	 * @var  array
	 */
	public static $search = [
		'id'
	];

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function fields(Request $request)
	{
		return [
			ID::make( 'Id',  'id')
				->rules('required')
				->sortable(),
			FilemanagerField::make( 'Banner Kv',  'banner_kv')
				->displayAsImage(),
			Number::make( 'Số nhà thuốc',  'nha_thuoc'),
			Number::make( 'Tỷ lệ trúng (%)',  'gioi_thieu_teaser'),
			NovaTrumbowyg::make( 'Nội dung Footer',  'footer')
				->hideFromIndex(),
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function cards(Request $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function filters(Request $request)
	{
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function lenses(Request $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function actions(Request $request)
	{
		return [];
	}
}
