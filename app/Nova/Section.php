<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Johnathan\NovaTrumbowyg\NovaTrumbowyg;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use NightKit\NovaElements\Fields\ElementRadio\ElementRadio;


class Section extends Resource
{
	/**
	 * The model the resource corresponds to.
	 *
	 * @var  string
	 */
	public static $model = 'App\Section';

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var  string
	 */
	public static $title = 'id';

	/**
	 * The columns that should be searched.
	 *
	 * @var  array
	 */
	public static $search = [
		'menu',
		'title',
		'teaser',
		'content'
	];

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function fields(Request $request)
	{
		return [
			ID::make( 'Id',  'id')
				->rules('required')
				->sortable()
			,
			Text::make('Tiêu đề menu', 'menu'),
			Text::make('Tiêu đề', 'title'),
			NovaTrumbowyg::make('Tóm tắt', 'teaser')
				->hideFromIndex(),
			NovaTrumbowyg::make('Nội dung xem thêm', 'content')
				->hideFromIndex(),
			Number::make('Vị trí hiển thị', 'weight')
			->sortable(),
			Number::make('Trạng thái', 'status'),
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function cards(Request $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function filters(Request $request)
	{
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function lenses(Request $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function actions(Request $request)
	{
		return [];
	}
}
