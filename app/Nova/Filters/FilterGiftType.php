<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class FilterGiftType extends Filter
{
	public $name = 'Giải thưởng';
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
    	$query->where('gift_type', $value);
        return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
        	'Không trúng' => 0,
	        'Giải đặc biệt' => 7,
	        'Giải nhất' => 1,
	        'Giải nhì' => 2,
	        'Giải ba' => 3,
	        'Giải tư' => 4,
	        'Giải năm' => 5,
	        'Giải KK' => 6,
        ];
    }
}
