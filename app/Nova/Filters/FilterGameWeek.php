<?php

namespace App\Nova\Filters;

use App\Gift;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class FilterGameWeek extends Filter
{
	public $name = 'Tuần';
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
    	$query->where('gift_id', $value);
        return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
    	$gifts = Gift::get();
    	$options = [];
    	foreach ($gifts as $gift){
    		$options[$gift->title] = $gift->id;
        }
        return $options;
    }
}
