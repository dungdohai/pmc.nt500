<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class FilterGiftOrNot extends Filter
{
	public $name = 'Trúng giải';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
    	if ($value == 0){
		    $query->where('gift_type', $value);
	    }else{
		    $query->where('gift_type', '>', 0);
	    }
        return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
        	'Không trúng' => 0,
	        'Trúng giải' => 1
        ];
    }
}
