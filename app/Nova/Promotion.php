<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Infinety\Filemanager\FilemanagerField;
use Inspheric\Fields\Url;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;


class Promotion extends Resource
{

	/**
	 * The model the resource corresponds to.
	 *
	 * @var  string
	 */
	public static $model = 'App\Promotion';

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var  string
	 */
	public static $title = 'id';

	/**
	 * The columns that should be searched.
	 *
	 * @var  array
	 */
	public static $search = [
		'id'
	];

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function fields(Request $request)
	{
		return [
			ID::make( 'Id',  'id')
				->rules('required')
				->sortable(),
			Text::make('Tiêu đề', 'title'),
			FilemanagerField::make( 'Banner',  'banner')
				->displayAsImage()
			,
			Url::make('Liên kết', 'link')
				->label('Liên kết')
				->clickableOnIndex(),
			Select::make('Trạng thái', 'status')->options([
				'1' => 'Hiển thị',
				'0' => 'Ẩn'
			])->displayUsingLabels(),
			Number::make('Vị trí', 'weight')
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function cards(Request $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function filters(Request $request)
	{
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function lenses(Request $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function actions(Request $request)
	{
		return [];
	}
}
