<?php

namespace App\Nova;

use App\Nova\Filters\FilterGameWeek;
use App\Nova\Filters\FilterGiftOrNot;
use App\Nova\Filters\FilterGiftType;
use App\Nova\Filters\GameDateFilter;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;


class Game extends Resource
{

	/**
	 * The model the resource corresponds to.
	 *
	 * @var  string
	 */
	public static $model = 'App\Game';

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var  string
	 */
	public static $title = 'id';

	public static function authorizedToCreate(Request $request)
	{
		return false;
	}

	public function authorizedToUpdate(Request $request)
	{
		return false;
	}

	public function authorizedToDelete(Request $request)
	{
		return true;
	}

	/**
	 * The columns that should be searched.
	 *
	 * @var  array
	 */
	public static $search = [
		'id',
		'user_phone',
		'user_code'
	];

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function fields(Request $request)
	{
		return [
			ID::make( 'Id',  'id')
				->rules('required')
				->sortable()
			,
			Text::make('Số điện thoại', 'user_phone'),
			Text::make('Mã may mắn', 'user_code'),
			Text::make('Họ và tên', 'fullname'),
			Text::make('Ngày sinh', 'birthday'),
            Select::make('Giới tính', 'gender')->options([
                '0' => 'n/a',
                '1' => 'Nam',
                '2' => 'Nữ',
            ])->displayUsingLabels(),
			Text::make('Email', 'email'),
			BelongsTo::make('Gift'),
			Select::make('Phần thưởng', 'gift_type')->options([
				'0' => 'Không trúng',
				'1' => 'Giải nhất',
				'2' => 'Giải nhì',
				'3' => 'Giải ba',
				'4' => 'Giải tư',
				'5' => 'Giải năm',
				'6' => 'Giải KK',
				'7' => 'Giải ĐB',
			])->displayUsingLabels(),
			Text::make('Địa chỉ IP', 'ip_address'),
			DateTime::make('Ngày chơi', 'created_at')->hideWhenUpdating()->hideWhenCreating()
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function cards(Request $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function filters(Request $request)
	{
		return [
		    new GameDateFilter(),
			new FilterGiftOrNot(),
			new FilterGiftType(),
			new FilterGameWeek()
		];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function lenses(Request $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function actions(Request $request)
	{
		return [
            (new DownloadExcel())->askForWriterType(),
		];
	}
}
