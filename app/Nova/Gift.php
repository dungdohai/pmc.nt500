<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;


class Gift extends Resource
{

	/**
	 * The model the resource corresponds to.
	 *
	 * @var  string
	 */
	public static $model = 'App\Gift';

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var  string
	 */
	public static $title = 'title';

	/**
	 * The columns that should be searched.
	 *
	 * @var  array
	 */
	public static $search = [
		'id'
	];

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function fields(Request $request)
	{
		return [
			ID::make( 'Id',  'id')
				->rules('required')
				->sortable()
			,
			Text::make('Tiêu đề', 'title'),
			Date::make('Từ ngày', 'valid_from'),
			Date::make('Đến ngày', 'valid_to'),
			Number::make( 'SL ĐB',  's_gift_quantity'),
			Number::make( 'SL G1',  'first_gift_quantity'),
			Number::make( 'SL G2',  'second_gift_quantity'),
			Number::make( 'SL G3',  'third_gift_quantity'),
			Number::make( 'SL G4',  'fourth_gift_quantity'),
			Number::make( 'SL G5',  'fifth_gift_quantity'),
			Number::make( 'SL KK',  'kk_gift_quantity'),
			Number::make( 'ĐB',  's_gift_count')->hideWhenCreating()->hideWhenUpdating(),
			Number::make( 'G1',  'first_gift_count')->hideWhenCreating()->hideWhenUpdating(),
			Number::make( 'G2',  'second_gift_count')->hideWhenCreating()->hideWhenUpdating(),
			Number::make( 'G3',  'third_gift_count')->hideWhenCreating()->hideWhenUpdating(),
			Number::make( 'G4',  'fourth_gift_count')->hideWhenCreating()->hideWhenUpdating(),
			Number::make( 'G5',  'fifth_gift_count')->hideWhenCreating()->hideWhenUpdating(),
			Number::make( 'KK',  'kk_gift_count')->hideWhenCreating()->hideWhenUpdating()
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function cards(Request $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function filters(Request $request)
	{
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function lenses(Request $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param    \Illuminate\Http\Request  $request
	 * @return  array
	 */
	public function actions(Request $request)
	{
		return [];
	}
}
