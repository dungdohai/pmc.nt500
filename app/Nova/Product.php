<?php

namespace App\Nova;

use Infinety\Filemanager\FilemanagerField;
use Inspheric\Fields\Url;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Product';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title'
    ];

    public static function authorizedToCreate(Request $request)
    {
    	return true;
    }

    public function authorizedToUpdate(Request $request)
    {
    	return true;
    }

    public function authorizedToDelete(Request $request)
    {
    	return true;
    }

	/**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
	        Text::make('Tiêu đề', 'title')
		        ->rules('required'),
	        FilemanagerField::make( 'Hình ảnh',  'image')
		        ->displayAsImage()
		        ->rules('required')
	        ,
	        Url::make('Liên kết', 'link')
		        ->label('Liên kết')
		        ->rules('required')
		        ->clickableOnIndex(),
	        Number::make('Giá bán', 'price')
		        ->rules('required'),
	        Number::make('Giá gốc', 'price_original'),
	        Number::make('% giảm giá', 'discount')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
