<?php


namespace App\Services;


class Lib{
    public static function response($status = true, $message = '', $data = []){
        return [
            'status' => $status,
            'data' => $data,
            'message' => $message
        ];
    }
}
