<?php


namespace App\Services;


use GuzzleHttp\Client;

class PmcApi{
    private $_url, $_campaignId;

    public function __construct(){
        $this->_url = config('app.api_url');
        $this->_campaignId = config('app.api_campaign_id');
    }

    private function _getHeader(){
        return [
            'pharmacity-partner-api-token' => 'asWSoeWJbCZlk6I8ttBKyUq5lChIhP1m'
        ];
    }

    private function _call($method, $params){
        $client = new Client();
        $response = $client->request(
            $method,
            $this->_url,
            [
                "headers" => $this->_getHeader(),
                "form_params" => $params,
                "http_errors" => false
            ]
        );

        if ($response->getStatusCode() === 200 && ($datas = json_decode($response->getBody()->getContents()))){
            if (isset($datas->errors)){
                $error = array_pop($datas->errors);
                return [
                    'status' => false,
                    'data' => $error->message
                ];
            }else{
                return [
                    'status' => true,
                    'data' => data_get($datas, 'data')
                ];
            }
        }

        return [
            'status' => false,
            'data' => 'Lỗi kết nối đến hệ thống'
        ];
    }

    public function getCode($phone){
        $params = [
            "query" => 'mutation getEventCustomerProfile($phone:String!,$campaign_id:Int!){getEventCustomerProfile(phone:$phone,campaign_id:$campaign_id){codes{code,times}}}',
            "variables" => [
                "phone" => $phone,
                "campaign_id" => (int) $this->_campaignId
            ]
        ];

        $response = $this->_call("POST", $params);
        if ($response['status']){
            return data_get($response, 'data.getEventCustomerProfile.codes', []);
        }

        return [];
    }
}
