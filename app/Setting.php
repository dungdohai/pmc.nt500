<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //

	static function getWinRate(){
		$setting = self::orderBy('id', 'desc')->first();
		return $setting ? $setting->gioi_thieu_teaser : 0;
	}
}
