<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('home');
Route::get('/iphone-xs-max', 'PageController@thele')->name('thele');
Route::post('/api', 'PageController@api')->name('api');
Route::any('/api/search', 'PageController@apiSearch')->name('api_search');
Route::get('/check', 'PageController@check')->name('check');


Route::post('/api/check', 'GameController@checkCode')->name('api.check');
Route::post('/api/lucky', 'GameController@lucky')->name('api.lucky');
