$.fn.extend({
    animateCss: function(animationName, callback) {
        var animationEnd = (function(el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') callback();
        });

        return this;
    },
});

function attachReadmore(){
    if ($('.prd-link').length > 0){
        return;
    }

    var el = $('.prd-list .col-6:last-child .prd-item');
    var prd = $('<div class="col-6 col-md-2dot4"><div class="prd-link bg-img rounded d-flex align-items-center my-3"><a href="https://www.pharmacity.vn/he-thong-cua-hang/?utm_source=luckydraw&utm_medium=landing-page&utm_campaign=mgwc-june2020" target="_blank" class="text-center rounded text-primary">Tìm nhà thuốc<br/><img class="img-fluid" src="https://blackmores.pharmacity.vn/images/official/logo-pmc.png"/> <br/>gần nhất</a></div></div>');
    $('.prd-link', prd).height(el.outerHeight());
    prd.appendTo('.prd-list');
}

$(document).ready(function (){
    init();

    $('[data-toggle="datepicker"]').datepicker({
        format: 'dd/mm/yyyy',
        monthsShort: ['Th 01', 'Th 02', 'Th 03', 'Th 04', 'Th 05', 'Th 06', 'Th 07', 'Th 08', 'Th 09', 'Th 10', 'Th 11', 'Th 12'],
        startView: 2,
        autoHide: true
    });

    Stickyfill.add($('.sticky'));
    var img = $('.prd-list .col-6:last-child img');
    if (img.prop('complete')) {
        attachReadmore();
    } else {
        img.load(function(){
            attachReadmore();
        });
    }

    $.scrollIt({
        topOffset: -150,
        onPageChange: function (id){
            $('#mainNav a').removeClass('btn-danger');
            var obj = $('#mainNav').find('[data-scroll-nav="'+id+'"]');
            $('a', obj).addClass('btn-danger');
        }
    });

    $('#luckyModal img').click(function (e){
        var $modal = $('#luckyModal');
        $('.loading', $modal).removeClass('d-none');

        e.preventDefault();

        var user_phone = $('#userPhone').val();
        var user_code = $('#userCode').val();
        var fullname = $('#userFullname').val();
        var birthday = $('#userBirthday').val();
        var email = $('#userEmail').val();
        var gender = parseInt($('#userGender').val());

        if (user_phone.length === 0){
            return alert('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        if (user_code.length === 0){
            return alert('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        if (fullname.length === 0){
            return alert('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        if (birthday.length === 0){
            return alert('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        $('#modalMessage, #modalErrors').addClass('d-none');

        axios.post(
            '/api/lucky',
            {
                phone: user_phone,
                code: user_code,
                fullname: fullname,
                birthday: birthday,
                email: email,
                gender: gender
            }
        ).then(function (response) {
            if (response.data.status){
                $('#modalMessage').html(response.data.message).removeClass('d-none');
            }else{
                $('#modalErrors').html(response.data.message).removeClass('d-none');
            }
        }).catch(function (error) {
            showErrors('Có lỗi trong quá trình xử lý, vui lòng thử lại sau!');
        }).then(function () {
            $('.loading', $modal).addClass('d-none');
        });
    });

    $('#loginForm').submit(function (e){
        e.preventDefault();

        var phone = $('#inputPhone').val();
        var password = $('#inputPassword').val();

        if (phone.length === 0 || password.length === 0){
            return alert('Bạn vui lòng nhập đầy đủ các thông tin!');
        }

        $('#loginForm .errors').addClass('d-none');

        $.ajax({
            type: 'POST',
            url: 'https://eapi.pharmacity.vn/graphql',
            dataType: 'json',
            async: false,
            data: {
                'query': "mutation createnewextracare($phone: String!, $password: String!, $re_password: String!){ registerWithPhone(phone: $phone, password: $password, re_password: $re_password){ id, db_id, phone, api_token }}",
                "variables": {
                    "phone": phone,
                    "password": password,
                    "re_password": password
                }
            },
            success: function(data) {
                if (data.errors){
                    var msg = '';

                    if (data.errors[0].message == 'validation' && data.errors[0].validation){
                        for (var key in data.errors[0].validation){
                            if (msg.length == 0){
                                msg = '<div>' + data.errors[0].validation[key][0] + '</div>'
                            }
                        }
                    }else{
                        msg = 'Số điện thoại đã được đăng ký tài khoản ExtraCare';
                    }


                    $('#loginForm .errors').removeClass('d-none').html(msg);
                    return false;
                }

                // ga('send', 'event', 'MauHetBenh', 'register', 'Register Success', phone);
                alert('Chúc mừng bạn đã đăng ký thành công!');
            }
        });
    });

    $('#freeCode').click(function (){
        $('#boxLike').remove();

        $('#userCode').val('FB' + $.now());

        showMessage('Bấm "NHẬN LƯỢT MỞ QUÀ" để nhận 01 lượt chơi miễn phí!');

        $('#btnSubmit').removeClass('d-none');
    });

    $('#btnFilter').click(function (){
        var filter_phone = $('#filterPhone').val();

        if (filter_phone.length === 0){
            $('#tableResult').removeClass('d-none');
            $('#tableResult tbody tr').removeClass('d-none');
            return alert('Vui lòng nhập vào số điện thoại cần tra cứu!');
        }

        $('#filterMessage').addClass('d-none');

        axios.post(
            '/api/search',
            {
                phone: filter_phone
            }
        ).then(function (response){

            if (response.data.length === 0){
                $('#tableResult').addClass('d-none');
                $('#filterMessage').removeClass('d-none');
            }else{
                $('#tableResult').removeClass('d-none');
                $('#tableResult tbody tr').addClass('d-none');

                for (var i in response.data){
                    const r = response.data[i];
                    $('#tableResult').append('<tr class="text-center"><td>1</td><td>'+r.user_phone+'</td><td>'+r.created_at+'</td><td>'+r.gift_title+'</td><td>'+r.gift_name+'</td></tr>');
                }
            }
        });
    });

    $('#gameForm').submit(function(e){
        e.preventDefault();

        var user_phone = $('#userPhone').val();
        var user_code = $('#userCode').val();
        var fullname = $('#userFullname').val();
        var birthday = $('#userBirthday').val();
        var email = $('#userEmail').val();

        if (user_phone.length === 0){
            return showErrors('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        if (user_code.length === 0){
            return showErrors('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        if (fullname.length === 0){
            return showErrors('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        if (birthday.length === 0){
            return showErrors('Bạn vui lòng điền đầy đủ thông tin để tham gia bốc thăm may mắn.');
        }

        $('#btnSubmit').attr('disabled', true);
        $('#loading').removeClass('d-none');
        $('#formMessage, #formInfo, #formError').addClass('d-none');

        var params = {
            phone: user_phone,
            code: user_code,
            fullname: fullname,
            birthday: birthday,
            email: email,
        };

        axios.post(
            '/api/check',
            params
        )
            .then(function (response) {
                if (response.data.status){
                    $('#modalMessage, #modalErrors').addClass('d-none');
                    $('#luckyModal').modal();
                }else{
                    showErrors(response.data.message);
                }
            })
            .catch(function (error) {
                showErrors('Có lỗi trong quá trình xử lý, vui lòng thử lại sau!');
            })
            .then(function () {
                $('#loading').addClass('d-none');
                $('#btnSubmit').removeAttr('disabled');
            });
    });

    $('.btn-fb-share').click(function (e){
        e.preventDefault();

        FB.ui({
            method: 'share',
            href: 'https://www.facebook.com/PharmacityVN/'
        }, function(response){
            console.log(response);
        });
    });

    $('#btnShowResult img').hover(
        function (){
            $(this).animateCss('pulse');
        },
        function (){
            // $(this).parent('span').removeClass('hover');
        }
    );

    $('.slick').slick({
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });
});

function showErrors(msg){
    $('#formErrors').removeClass('d-none').html(msg);

    $('#formMessage, #formInfo').addClass('d-none');
}

function showMessage(msg){
    $('#formMessage').removeClass('d-none').html(msg);

    $('#formErrors, #formInfo').addClass('d-none');
}

function showInfo(msg){
    $('#formInfo').removeClass('d-none').html(msg);
    $('#formErrors, #formMessage').addClass('d-none');
}
