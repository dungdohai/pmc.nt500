(function (lib, img, cjs, ss, an) {

    var p; // shortcut to reference prototypes
    lib.webFontTxtInst = {};
    var loadedTypekitCount = 0;
    var loadedGoogleCount = 0;
    var gFontsUpdateCacheList = [];
    var tFontsUpdateCacheList = [];
    lib.ssMetadata = [
        {name:"banner_atlas_", frames: [[154,406,166,58],[400,0,295,479],[0,0,398,404],[0,481,424,271],[0,406,152,67],[528,481,90,92],[426,481,100,100]]}
    ];



    lib.updateListCache = function (cacheList) {
        for(var i = 0; i < cacheList.length; i++) {
            if(cacheList[i].cacheCanvas)
                cacheList[i].updateCache();
        }
    };

    lib.addElementsToCache = function (textInst, cacheList) {
        var cur = textInst;
        while(cur != exportRoot) {
            if(cacheList.indexOf(cur) != -1)
                break;
            cur = cur.parent;
        }
        if(cur != exportRoot) {
            var cur2 = textInst;
            var index = cacheList.indexOf(cur);
            while(cur2 != cur) {
                cacheList.splice(index, 0, cur2);
                cur2 = cur2.parent;
                index++;
            }
        }
        else {
            cur = textInst;
            while(cur != exportRoot) {
                cacheList.push(cur);
                cur = cur.parent;
            }
        }
    };

    lib.gfontAvailable = function(family, totalGoogleCount) {
        lib.properties.webfonts[family] = true;
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
        for(var f = 0; f < txtInst.length; ++f)
            lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);

        loadedGoogleCount++;
        if(loadedGoogleCount == totalGoogleCount) {
            lib.updateListCache(gFontsUpdateCacheList);
        }
    };

    lib.tfontAvailable = function(family, totalTypekitCount) {
        lib.properties.webfonts[family] = true;
        var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];
        for(var f = 0; f < txtInst.length; ++f)
            lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);

        loadedTypekitCount++;
        if(loadedTypekitCount == totalTypekitCount) {
            lib.updateListCache(tFontsUpdateCacheList);
        }
    };
// symbols:



    (lib.bg_quakhung_02 = function() {
        this.spriteSheet = ss["banner_atlas_"];
        this.gotoAndStop(0);
    }).prototype = p = new cjs.Sprite();



    (lib.bg_quakhung_03 = function() {
        this.spriteSheet = ss["banner_atlas_"];
        this.gotoAndStop(1);
    }).prototype = p = new cjs.Sprite();



    (lib.bg_quakhung_06 = function() {
        this.spriteSheet = ss["banner_atlas_"];
        this.gotoAndStop(2);
    }).prototype = p = new cjs.Sprite();



    (lib.bg_quakhung_08 = function() {
        this.spriteSheet = ss["banner_atlas_"];
        this.gotoAndStop(3);
    }).prototype = p = new cjs.Sprite();



    (lib.bg_quakhung_11 = function() {
        this.spriteSheet = ss["banner_atlas_"];
        this.gotoAndStop(4);
    }).prototype = p = new cjs.Sprite();



    (lib.gt1 = function() {
        this.spriteSheet = ss["banner_atlas_"];
        this.gotoAndStop(5);
    }).prototype = p = new cjs.Sprite();



    (lib.gt2 = function() {
        this.spriteSheet = ss["banner_atlas_"];
        this.gotoAndStop(6);
    }).prototype = p = new cjs.Sprite();



    (lib.Tween8 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.gt2();
        this.instance.parent = this;
        this.instance.setTransform(-45,-45,0.9,0.9);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-45,-45,90,90);


    (lib.Tween7 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.gt1();
        this.instance.parent = this;
        this.instance.setTransform(-40.5,-41.4,0.9,0.9);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-40.5,-41.4,81,82.8);


    (lib.Tween6 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.bg_quakhung_08();
        this.instance.parent = this;
        this.instance.setTransform(-212,-135.5);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-212,-135.5,424,271);


    (lib.Tween5 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.bg_quakhung_03();
        this.instance.parent = this;
        this.instance.setTransform(-147.5,-239.5);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-147.5,-239.5,295,479);


    (lib.Tween4 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.bg_quakhung_02();
        this.instance.parent = this;
        this.instance.setTransform(-83,-29);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-83,-29,166,58);


    (lib.Tween3 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.bg_quakhung_06();
        this.instance.parent = this;
        this.instance.setTransform(-199,-202);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-199,-202,398,404);


    (lib.Tween1 = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.bg_quakhung_11();
        this.instance.parent = this;
        this.instance.setTransform(-76,-33.5);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-76,-33.5,152,67);


    (lib.mc = function(mode,startPosition,loop) {
        if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

        // timeline functions:
        this.frame_69 = function() {
            this.stop();
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

        // Layer 9
        this.instance = new lib.Tween8("synched",0);
        this.instance.parent = this;
        this.instance.setTransform(489,9,0.006,0.006,0,0,0,0,9);
        this.instance._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(15).to({_off:false},0).to({regY:0,scaleX:1.2,scaleY:1.2},2).to({scaleX:1,scaleY:1,y:15.2},5).wait(48));

        // Layer 10
        this.instance_1 = new lib.Tween7("synched",0);
        this.instance_1.parent = this;
        this.instance_1.setTransform(574.5,35.4,0.006,0.006,0,0,0,8.1,0);
        this.instance_1._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(12).to({_off:false},0).to({regX:0,scaleX:1.2,scaleY:1.2},2).to({scaleX:1,scaleY:1,y:41.6},5).wait(51));

        // bg_quakhung_02.png
        this.instance_2 = new lib.Tween4("synched",0);
        this.instance_2.parent = this;
        this.instance_2.setTransform(1063,33);
        this.instance_2.alpha = 0;
        this.instance_2._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(20).to({_off:false},0).to({alpha:1},9,cjs.Ease.get(1)).to({startPosition:0},40).wait(1));

        // bg_quakhung_11.png
        this.instance_3 = new lib.Tween1("synched",0);
        this.instance_3.parent = this;
        this.instance_3.setTransform(1056,398.2,1.5,1.5);
        this.instance_3.alpha = 0;
        this.instance_3._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(15).to({_off:false},0).to({scaleX:1,scaleY:1,y:357.5,alpha:1},9,cjs.Ease.get(1)).to({startPosition:0},45).wait(1));

        // bg_quakhung_08.png
        this.instance_4 = new lib.Tween6("synched",0);
        this.instance_4.parent = this;
        this.instance_4.setTransform(920,250.2,1.5,1.5);
        this.instance_4.alpha = 0;
        this.instance_4._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(10).to({_off:false},0).to({scaleX:1,scaleY:1,y:209.5,alpha:1},9,cjs.Ease.get(1)).to({startPosition:0},50).wait(1));

        // bg_quakhung_03.png
        this.instance_5 = new lib.Tween5("synched",0);
        this.instance_5.parent = this;
        this.instance_5.setTransform(530.5,170.2,1.5,1.5);
        this.instance_5.alpha = 0;
        this.instance_5._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off:false},0).to({scaleX:1,scaleY:1,y:239.5,alpha:1},9,cjs.Ease.get(1)).to({startPosition:0},55).wait(1));

        // bg_quakhung_06.png
        this.instance_6 = new lib.Tween3("synched",0);
        this.instance_6.parent = this;
        this.instance_6.setTransform(199,286.4,1.5,1.5);
        this.instance_6.alpha = 0;

        this.timeline.addTween(cjs.Tween.get(this.instance_6).to({scaleX:1,scaleY:1,y:205,alpha:1},9,cjs.Ease.get(1)).to({startPosition:0},60,cjs.Ease.get(1)).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-99.5,-16.6,597,606);


// stage content:
    (lib.banner = function(mode,startPosition,loop) {
        this.initialize(mode,startPosition,loop,{});

        // Layer 1
        this.instance = new lib.mc();
        this.instance.parent = this;
        this.instance.setTransform(407.2,192.6,0.7,0.7,0,0,0,573.1,239.5);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(343.8,207.3,417.9,424.2);
// library properties:
    lib.properties = {
        width: 815,
        height: 388,
        fps: 24,
        color: "#666666",
        opacity: 1.00,
        webfonts: {},
        manifest: [
            {src:"images/banner_atlas_.png", id:"banner_atlas_"}
        ],
        preloads: []
    };




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;
