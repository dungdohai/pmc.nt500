(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.banner_store500_giai1 = function() {
	this.initialize(img.banner_store500_giai1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,271,203);


(lib.banner_store500_giai2 = function() {
	this.initialize(img.banner_store500_giai2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,271,169);


(lib.st500_item1_vangsang = function() {
	this.initialize(img.st500_item1_vangsang);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,525,437);


(lib.st500_item2_vangsang2 = function() {
	this.initialize(img.st500_item2_vangsang2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,525,437);


(lib.st500_item3_vangsang3 = function() {
	this.initialize(img.st500_item3_vangsang3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,525,437);


(lib.st500_item_bangronchaomung = function() {
	this.initialize(img.st500_item_bangronchaomung);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,406,160);


(lib.st500_item_icon500 = function() {
	this.initialize(img.st500_item_icon500);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,421,240);


(lib.st500_item_iconboom = function() {
	this.initialize(img.st500_item_iconboom);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,141,158);


(lib.st500_item_nhathuoc = function() {
	this.initialize(img.st500_item_nhathuoc);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,234,160);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.banner_store500_giai2();
	this.instance.parent = this;
	this.instance.setTransform(-135.5,-84.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-135.5,-84.5,271,169);


(lib.Tween25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.st500_item1_vangsang();
	this.instance.parent = this;
	this.instance.setTransform(-263,218,1,1,0,180,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-263,-219,525,437);


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.st500_item_nhathuoc();
	this.instance.parent = this;
	this.instance.setTransform(-117,-80);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-117,-80,234,160);


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.st500_item_icon500();
	this.instance.parent = this;
	this.instance.setTransform(-210.5,-120);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-210.5,-120,421,240);


(lib.Tween22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.st500_item_iconboom();
	this.instance.parent = this;
	this.instance.setTransform(-70.5,-79);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-70.5,-79,141,158);


(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.st500_item2_vangsang2();
	this.instance.parent = this;
	this.instance.setTransform(-262.5,-218.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-262.5,-218.5,525,437);


(lib.Tween11 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","rgba(255,255,255,0)"],[0,0.533,1],-40.1,0,40.2,0).s().p("AmRUxMAAAgphIMjAAMAAAAphg");
	this.shape.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40.1,-132.8,80.3,265.8);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.banner_store500_giai1();
	this.instance.parent = this;
	this.instance.setTransform(-126,-108);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-126,-108,271,203), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.st500_item3_vangsang3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,525,437), null);


(lib.mc_bangron = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A+3LuIAA3bMA9uAAAIAAXbgAvFCMQgCAAAUCgQAUChgDAAIOOhQQONA/ALACIAAAAIAAAAQgCgCAMiXQANiZgDAAQgFAAu1ipQgGABudCog");
	mask.setTransform(208.5,78.1);

	// Layer 1
	this.instance = new lib.st500_item_bangronchaomung();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.mc_bangron, new cjs.Rectangle(10.9,3.1,395.1,150), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(52));

	// Isolation Mode (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AoSZPQmtgzgUgBQAFgCAggaQAkgdABACQADADAHkwQgDgDgRi6QjciLl0A6Qh1ASh1AkIheAgQADgEA7i4QA6ixAFgKQgKgFiDh0QiPh/gBgRQgCgPDlg4QDcg3gMgFQgugThXhJQh0hfgLhDQgLhFArg8QAegpArgZQAWgMBGALQCrAbBSAJQChASBUhlQAkgsAdhdQALghgSgXQgVgag+gXQg3gUjEAPQi9AOAFAGIiRiHQACABA1nFQA2nHABACQADAFD1AHQEPAICSgKQCPgLBmA6QAzAdAYAiQABABAYgoQAigyAwgpQCaiHD0gKQErA1CpD3QAhAxA0BnQAeA5ALgOIA9hSQA4hJA4g1QCvilCxASQC2ASB/BrQAtAlBKBPQBjBqAnAmQB9B7gFEgQgBBKgICKQgEBuASAvQB+AnA2B5QAbA9ADA2QACACAKgFIAmgWQAdgRAPATQAQASgFAyQgEAjgrAeQgoAcABAaQABAhg/CNQggBHgfBBQAFAIgHAYQgJAggiAVQggAUgrACQgkgDABABQABAChOA9QhPA8ABACIC5GaQADAEgygRQg7gVhAgfQg+gdj7gDQj0gDAFAHQABAChEAQQhFAQABABIhmAyQABACgJDhQgJDhABABQABACARAIIARAKQABACACAXQABAYABABIskB+IAAAAQgOAAm0g1gAPTuzQg7BcgiCUQggCRAHCFQAHCOA0BEQCzgHAcmcQAIh+gHibIgIh+IAAAAIAAAAIgGAAQhIAAg/BigAkHsxQgmC8gBDEIgBAZQgBAcAHASQAYA7BigzQCeh+ASlkQAFhugHh6IgJhhIAAAAIAAAAQgMgCgMAAQieAAhHFegA7CpfIAAAAIAAAAg");
	mask.setTransform(581.7,341.5);

	// Layer 6
	this.instance = new lib.Tween11("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(341.2,185.3,1.324,2.766,30);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(57).to({_off:false},0).to({scaleX:1,scaleY:1.3,x:831.5,y:427.8},56,cjs.Ease.get(1)).to({_off:true},1).wait(51));

	// Layer 9
	this.instance_1 = new lib.Tween11("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(265.6,187.2,2.581,2.581,30,0,0,0.2,-0.1);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:0.1,regY:-0.2,scaleX:2.55,scaleY:2.47,x:912,y:457.6},63,cjs.Ease.get(1)).to({_off:true},2).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(382.1,174.7,144.2,333.6);


(lib.mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_64 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(64).call(this.frame_64).wait(15));

	// Layer 2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-85.3,152.5,1,1,0,0,0,199,202);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(64).to({_off:false},0).to({_off:true},11).wait(4));

	// st500_item2_vangsang2.png
	this.instance_1 = new lib.Tween21("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(280.5,254.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(12).to({_off:false},0).to({alpha:1},9).to({_off:true},54).wait(4));

	// st500_item_bangronchaomung.png
	this.instance_2 = new lib.mc_bangron();
	this.instance_2.parent = this;
	this.instance_2.setTransform(284,360,0.402,0.402,0,0,0,203,80);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({_off:false},0).to({regX:203.2,regY:80.2,scaleX:1.19,scaleY:1.19,x:284.2,y:339.1,alpha:0.551},5,cjs.Ease.get(1)).to({regX:203,regY:80,scaleX:1,scaleY:1,x:284,y:340,alpha:1},4).wait(63));

	// st500_item_iconboom.png
	this.instance_3 = new lib.Tween22("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(445.5,269);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(14).to({_off:false},0).to({y:289,alpha:1},5,cjs.Ease.get(1)).to({_off:true},56).wait(4));

	// st500_item_icon500.png
	this.instance_4 = new lib.Tween23("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(291.5,353,0.429,0.429,0,0,0,0,120);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(14).to({_off:false},0).to({regX:0.1,regY:120.2,scaleX:1,scaleY:1,x:291.6,y:333.2},5).to({regX:0,regY:140.2,scaleX:1,scaleY:1,x:291.5,y:373.2},4).to({startPosition:0},4).wait(52));

	// st500_item_nhathuoc.png
	this.instance_5 = new lib.Tween24("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(287,368,0.174,0.174);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off:false},0).to({scaleX:1.17,scaleY:1.17,y:388,alpha:1},5).to({scaleX:1,scaleY:1},4).to({startPosition:0},4).wait(61));

	// st500_item3_vangsang3.png
	this.instance_6 = new lib.Symbol6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(280.5,254.5,0.55,0.55,0,0,0,262.6,218.6);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(9).to({_off:false},0).to({regX:262.5,regY:218.5,scaleX:1,scaleY:1,alpha:1},9,cjs.Ease.get(1)).to({_off:true},57).wait(4));

	// Symbol 7
	this.instance_7 = new lib.Symbol7();
	this.instance_7.parent = this;
	this.instance_7.setTransform(680.5,223.5);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(24).to({_off:false},0).to({scaleX:1.2,scaleY:1.2,y:171.5,alpha:1},6,cjs.Ease.get(1)).to({_off:true},45).wait(4));

	// banner_store500_giai2.png
	this.instance_8 = new lib.Tween26("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(670.5,415.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(28).to({_off:false},0).to({regX:0.1,regY:0.1,scaleX:1.1,scaleY:1.1,y:385.5,alpha:1},6,cjs.Ease.get(1)).to({_off:true},41).wait(4));

	// st500_item1_vangsang.jpg
	this.instance_9 = new lib.Tween25("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(280.5,254.5);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({regX:0.1,regY:0.1,x:280.6,y:248.5,alpha:1},9).to({_off:true},66).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(13,28,763,451);


// stage content:
(lib.banner = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mc();
	this.instance.parent = this;
	this.instance.setTransform(568,222.6,1,1,0,0,0,573.1,239.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(419.8,258.5,525,437);
// library properties:
lib.properties = {
	width: 815,
	height: 480,
	fps: 20,
	color: "#F06830",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/banner_store500_giai1.jpg", id:"banner_store500_giai1"},
		{src:"images/banner_store500_giai2.png", id:"banner_store500_giai2"},
		{src:"images/st500_item1_vangsang.jpg", id:"st500_item1_vangsang"},
		{src:"images/st500_item2_vangsang2.png", id:"st500_item2_vangsang2"},
		{src:"images/st500_item3_vangsang3.png", id:"st500_item3_vangsang3"},
		{src:"images/st500_item_bangronchaomung.png", id:"st500_item_bangronchaomung"},
		{src:"images/st500_item_icon500.png", id:"st500_item_icon500"},
		{src:"images/st500_item_iconboom.png", id:"st500_item_iconboom"},
		{src:"images/st500_item_nhathuoc.png", id:"st500_item_nhathuoc"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;