<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGameTableUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('games', function (Blueprint $table){
            $table->string('fullname', 50);
            $table->string('birthday', 50);
            $table->string('email', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('games', function (Blueprint $table){
            $table->dropColumn('fullname');
            $table->dropColumn('birthday');
            $table->dropColumn('email');
        });
    }
}
