<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', '255');
            $table->date('valid_from');
            $table->date('valid_to');
            $table->tinyInteger('first_gift_quantity')->default(0);
            $table->tinyInteger('second_gift_quantity')->default(0);
            $table->tinyInteger('third_gift_quantity')->default(0);
            $table->tinyInteger('kk_gift_quantity')->default(0);

	        $table->tinyInteger('first_gift_count')->default(0);
	        $table->tinyInteger('second_gift_count')->default(0);
	        $table->tinyInteger('third_gift_count')->default(0);
	        $table->tinyInteger('kk_gift_count')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
