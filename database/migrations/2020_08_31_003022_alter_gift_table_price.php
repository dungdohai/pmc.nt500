<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGiftTablePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('gifts', function (Blueprint $table){
            $table->integer('fourth_gift_quantity')->default(0)->after('third_gift_quantity');
            $table->integer('fifth_gift_quantity')->default(0)->after('fourth_gift_quantity');

            $table->integer('fourth_gift_count')->default(0)->after('third_gift_count');
            $table->integer('fifth_gift_count')->default(0)->after('fourth_gift_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('gifts', function (Blueprint $table){
            $table->dropColumn('fourth_gift_quantity');
            $table->dropColumn('fifth_gift_quantity');
            $table->dropColumn('fourth_gift_count');
            $table->dropColumn('fifth_gift_count');
        });
    }
}
