<section id="sectionRegister" data-scroll-index="1" class="mt-5">
    <div class="container">
        <form id="loginForm" class="py-5 rounded">
            <div class="row">
                <div class="col-12 col-md-5 offset-md-7">
                    <div class="p-3 px-md-5">
                        <h2 class="text-title mb-3 d-none d-md-block">Đăng ký ngay Extracare để trải nghiệm!</h2>
                        <h5 class="text-title mb-3 d-block d-md-none">Đăng ký ngay Extracare để trải nghiệm!</h5>
                        <div class="mb-3 text-danger d-none errors"></div>
                        <div class="form-group">
                            <label for="inputPhone" class="sr-only col-form-label">Số điện thoại</label>
                            <input type="number" class="form-control form-control-lg" id="inputPhone" autocomplete="off" placeholder="Số điện thoại">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="sr-only col-form-label">Mật khẩu</label>
                            <input type="password" class="form-control form-control-lg" id="inputPassword" autocomplete="off" placeholder="Mật khẩu">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-block btn-success px-5">Đăng ký</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>