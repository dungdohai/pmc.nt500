<div class="col-6 col-md-2dot4">
    <div class="prd-item bg-img my-3 p-3 rounded border">
        <div><a href="{!! $product->link !!}" target="_blank"><img src="{{ Storage::url($product->image) }}" class="w-100 mb-3 mx-auto" /></a></div>
        <h6 class="mb-3">{{ $product->title }}</h6>
        <div class="row mb-3">
            <div class="col-6 text-success strong">{{ number_format($product->price) }}đ</div>
            @if (!empty($product->price_original))
                <div class="col-6 text-right"><del>{{ number_format($product->price_original) }}đ</del></div>
            @endif
        </div>
        <div class="mb-2">
            <a href="{!! $product->link !!}" target="_blank" class="btn btn-primary btn-circle btn-block strong">Mua ngay</a>
        </div>
    </div>
</div>
