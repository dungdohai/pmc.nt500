<div class="sticky">
    <div class="container-fluid">
        <ul id="mainNav" class="nav pt-2 px-2 px-md-0 justify-content-center">
            <li class="nav-item mr-2 mb-2">
                <a class="btn btn-lg btn-primary px-2 px-md-3" href="{{ route('thele') }}">Rinh iPhone XS Max 256 GB</a>
            </li>

            <li class="nav-item mr-2 mb-2" data-scroll-nav="0">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#videoSection">Mua Blackmores đến Pharmacity</a>
            </li>

            <li class="nav-item mr-2 mb-2" data-scroll-nav="1">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#sectionRegister">Đăng ký Extracare</a>
            </li>

            <li class="nav-item mr-2 mb-2" data-scroll-nav="2">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#sectionIntro">Ưu đãi đặc biệt</a>
            </li>

            <li class="nav-item mr-2 mb-2" data-scroll-nav="3">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#sectionProduct">Sản phẩm nổi bật</a>
            </li>

            <li class="nav-item mr-2 mb-2"  data-scroll-nav="4">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#sectionBlog">Blog sức khỏe</a>
            </li>
        </ul>
    </div>
</div>
