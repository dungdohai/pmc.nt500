<div class="header p-3">
    <div class="row">
        <div class="col-6 col-sm mr-sm-auto">
            <a href="{{ route('home') }}" class="align-middle"><img src="{{ Storage::url($config->logo) }}" class="img-fluid"/></a>
        </div>
        <div class="col-md-auto col-3 strong no-wrap">
            <a href="tel:18006821">Hotline miễn phí<br/>
            18006821</a>
        </div>
        <div class="col-md-auto col-3 strong no-wrap">
            <a href="https://www.pharmacity.vn/he-thong-cua-hang-pharmacity" target="_blank">Hệ thống<br/>
                137 nhà thuốc</a>
        </div>
    </div>
</div>

<div>
    <img src="{{ Storage::url($config->banner_1) }}" class="img-banner" />
</div>
