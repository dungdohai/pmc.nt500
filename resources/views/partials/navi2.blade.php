<div class="sticky">
    <div class="container">
        <ul id="mainNav" class="nav pt-2 px-2 px-md-0 justify-content-center">
            <li class="nav-item mr-2 mb-2">
                <a class="btn btn-lg btn-primary px-2 px-md-3" href="#gameSection">Trúng Vàng cùng Pharmacity</a>
            </li>

            <li class="nav-item mr-2 mb-2" data-scroll-nav="1">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#section-winners">Danh sách trúng thưởng</a>
            </li>

            <li class="nav-item mr-2 mb-2" data-scroll-nav="2">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#section-qna">Hỏi đáp</a>
            </li>
<!---
            <li class="nav-item mr-2 mb-2" data-scroll-nav="1">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#sectionRegister">Đăng ký Extracare</a>
            </li>
	
    		<li class="nav-item mr-2 mb-2" data-scroll-nav="3">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#sectionProduct">Sản phẩm nổi bật</a>
            </li>
             <li class="nav-item mr-2 mb-2"  data-scroll-nav="4">
                <a class="btn btn-lg btn-primary sliding-link px-2 px-md-3" href="#sectionBlog">Blog sức khỏe</a>
            </li>
---->
        </ul>
    </div>
</div>
<div id="winnerInfo" class="bg-success2 text-white text-center py-2 h5 mb-3 d-flex align-items-center justify-content-center">
    <img class="mr-3" src="{{ asset('images/gift-red.png') }}"/> Đã có <span class="text-warning strong bigsize mx-2 align-self-start">{{ number_format($gameCount) }}</span> khách hàng trúng giải
</div>
