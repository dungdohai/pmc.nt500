<div class="header p-2">
    <div class="row">
        <div class="col-6 col-sm mr-sm-auto">
            <a href="https://www.pharmacity.vn/" class="align-middle">
                <img src="{{ asset('images/logo-header.png') }}" class="logo_img-fluid" alt="Pharmacity Logo"/>
            </a>
        </div>
        <div class="col-md-auto col-3 strong no-wrap text-center" style="line-height: 1.2rem;">
            <a href="tel:18006821">Hotline miễn phí<br/>
            18006821 - 18002001</a>
        </div>
        <div class="col-md-auto col-3 strong no-wrap text-center" style="line-height: 1.2rem;">
            <a href="https://www.pharmacity.vn/he-thong-cua-hang/?utm_source=luckydraw&utm_medium=landing-page&utm_campaign=mgwc-june2020" target="_blank">Tìm cửa hàng <br />gần bạn</a>
        </div>
    </div>
</div>
