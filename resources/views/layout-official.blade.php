<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5C6S53P');</script>
    <!-- End Google Tag Manager -->
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:title" content="CHÀO MỪNG NHÀ THUỐC 500 | Pharmacity" />
    <meta property="og:image" content="{{ asset('images/1200x628-FB-EVENT-NT500.jpg') }}"/>

    <meta property="og:description" content="Nhận mã số may mắn với hoá đơn chỉ từ 120.000đ. Áp dụng từ ngày 01 – 30/09/2020" />

    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('fonts/vogue2/fonts.css') }}" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="{{ asset('js/library/slick/slick.css') }}" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/library/slick/slick-theme.css') }}" crossorigin="anonymous">



    <link rel="stylesheet" href="{{ asset('fonts/line-awesome/css/line-awesome.min.css') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('datepicker/datepicker.min.css') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/pmc.css?v=1.997') }}" crossorigin="anonymous">
	<title>CHÀO MỪNG NHÀ THUỐC 500 | Pharmacity</title>
    <meta name="description" content="Nhận mã số may mắn với hoá đơn chỉ từ 120.000đ. Áp dụng từ ngày 01 – 30/09/2020" data-react-helmet="true">

</head>
<body class="{{ isset($bodyClass) ? $bodyClass : '' }}">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5C6S53P"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@include('partials/header-official')

@yield('content')

@yield('footer')

<section id="footer" class="pt-3">
    <div class="container">
        <div class="row">
            <div class="col d-none d-md-block">
          <img src="{{ asset('images/logo-header.png') }}" class="img-fluid" alt="Pharmacity Logo"/>
            </div>
            <div class="col-12 col-md-auto">
                <div class="text-center mb-3 d-block d-sm-none">
                    <img src="{{ asset('images/icon-bct.png') }}" />
                </div>
                <div class="pl-5 pl-sm-0">
                    {!! $config->footer !!}
                </div>
            </div>
            <div class="col d-none d-md-block text-center">
                <div class="mb-3"><img src="{{ asset('images/icon-bct-sm.png') }}" /></div>
                <div><img src="{{ asset('images/icon-ssl.png') }}" /></div>
            </div>
            <div class="col d-none d-md-block">
                <div class="text-uppercase mb-3">
                    <small>Phương thức thanh toán</small><br/>
                    <img class="mt-1" src="{{ asset('images/icon-cod.png') }}">
                </div>
                {{--<div class="text-uppercase">--}}
                    {{--<small>Đơn vị vận chuyển</small>--}}
                    {{--<img class="mt-1" src="{{ asset('images/logo-nhattin.png') }}">--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</section>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset("js/library/jquery-latest.min.js") }}" ></script>
<script src="{{ asset("js/bootstrap/popper.min.js") }}"></script>
<script src="{{ asset("js/bootstrap/bootstrap.bundle.min.js") }}"></script>
<!--<script src="{{ asset("js/library/slick/slick.js") }}"></script> -->

<script src="{{ asset("slick/slick.js") }}"></script>
<script src="{{ asset("js/library/axios.min.js") }}"></script>
<script src="{{ asset("js/library/sweetalert2.all.min.js") }}"></script>
<script src="{{ asset("js/library/stickyfill.js") }}"></script>
<script src="{{ asset("js/scrollIt.min.js") }}"></script>
<script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>

<script src="{{ asset("datepicker/datepicker.min.js") }}"></script>
<script src="{{ asset("banner.js?v=1.997") }}"></script>
<script src="{{ asset("banner_pmc.js?v=1.997") }}"></script>
<script src="{{ asset("js/pmc.js?v=2.3") }}"></script>

<script src="https://sp.zalo.me/plugins/sdk.js"></script>

@yield('footerjs')
</body>
</html>
