@extends('layout-official')

@section('content')

    <section id="gameSection" class="px-md-0 text-white">
        <div class="container">
            <div class="row sm-gutters">
                <div class="col-md-9 order-md-last">
                    <div class="my-3 d-md-none text-center">
                        <img class="img-fluid w-75 mx-auto" src="{{ asset('images/official/text-game.png') }}" />
                    </div>

                    <div id="animation_container" class="d-lg-block d-none mt-lg-4">
                        <canvas id="canvas" width="815" height="480"></canvas>
                        <div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:815px; height:480px; display: block;">
                        </div>
                    </div>

 <div class="d-block d-lg-none">
                        <img class="img-fluid" src="{{ asset('images/official/banner_mobile.png') }}">
                    </div>
                </div>

                <div class="col-md-3 pt-md-3">
                    <div id="logoLucky" class="strong mb-3 d-none d-md-block">
                        <a href="javascript:void(0);"><img class="img-fluid" src="{{ asset('images/official/text-game.png') }}" /></a>
                    </div>

                    <form id="gameForm" action="{{ route('api') }}" class="mt-3">
                        <div class="form-group">
                            <label class="sr-only" for="userEmail">Số điện thoại</label>
                            <input type="number" class="form-control" id="userPhone" name="user_phone" placeholder="Nhập số điện thoại ExtraCare" value="{{ data_get($customer, 'phone') }}" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="userCode">Nhập mã</label>
                            <input type="text" class="form-control" id="userCode" name="user_code" placeholder="Nhập mã trên hoá đơn" value="" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="userFullname">Họ và tên</label>
                            <input type="text" class="form-control" id="userFullname" name="fullname" placeholder="Họ và tên" value="{{ data_get($customer, 'fullname') }}" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="userBirthday">Ngày sinh</label>
                            <input type="text" class="form-control" id="userBirthday" name="birthday" placeholder="Ngày sinh" data-toggle="datepicker" readonly="readonly" value="{{ data_get($customer, 'birthday') }}" />
                        </div>
{{--                       <div class="form-group">--}}
{{--                           <label class="sr-only" for="userGender">Giới tính</label>--}}
{{--                           <select class="form-control" name="gender" id="userGender">--}}
{{--                              <option value="0" selected>Giới tính</option>--}}
{{--                              <option value="1" @if (data_get($customer, 'gender') == 1) selected="selected" @endif>Nam</option>--}}
{{--                              <option value="2" @if (data_get($customer, 'gender') == 2) selected="selected" @endif>Nữ</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}



                        <div class="form-group">
                            <label class="sr-only" for="userEmail">Email (tuỳ chọn)</label>
                            <input type="text" class="form-control" id="userEmail" name="email" placeholder="Email (tuỳ chọn)" value="{{ data_get($customer, 'email') }}" />
                        </div>

                        <div id="loading" class="text-center text-primary rounded mb-3 p-3 d-none">
                            <img src="{{ asset('images/loading.svg') }}" />
                            Hệ thống đang xử lý, vui lòng đợi trong giây lát...
                        </div>

                        <div class="form-group">
                            <button id="btnSubmit" type="submit" class="btn btn-block btn-success">Nhận lượt mở quà</button>
                        </div>

                        <div class="mb-3 alert alert-danger d-none text-center" id="formErrors"></div>
                        <div class="mb-3 alert alert-success d-none text-center" id="formMessage"></div>
                        <div class="mb-3 alert alert-info d-none text-center" id="formInfo"></div>
                    </form>
                </div>
            </div>

    </section>
    @include('partials/navi2')

	 <div class="mb-3"></div>
    @foreach ($sections as $section)
        <section id="section-{{ $section->id }}" class="mb-3 mb-md-5" data-scroll-index="0">
            <div class="container">
                <h5 class="bg-success2 text-white px-3 py-2 strong mb-0 rounded">{{ $section->title }}</h5>
                <div class="border border-top-0 py-3 px-4 section-content bg-white">
                    {!! $section->teaser !!}

                    @if ($section->content !== null)
                        <div id="content-{{ $section->id }}" class="collapse">
                            {!! $section->content !!}
                        </div>
                        <div class="text-center">
                            <a href="#content-{{ $section->id }}" class="btn btn-outline-primary btn-toggle collapsed" data-toggle="collapse">
                                <span class="expand-text">Xem thêm</span>
                                <span class="collapse-text">Thu gọn</span>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </section>
    @endforeach
     <section id="section-winners" class="mb-3 mb-md-5" data-scroll-index="1">
        <div class="container">
            <h5 class="bg-success2 text-white px-3 py-2 strong mb-0 rounded">DANH SÁCH TRÚNG THƯỞNG</h5>
            <div class="border border-top-0 py-3 px-4 section-content bg-white">
                <div class="row mb-3">
                    <div class="col-lg-4 offset-lg-4">
                        <div class="input-group">
                            <input type="number" class="form-control" id="filterPhone" name="filter-phone" placeholder="Số điện thoại">
                            <span class="input-group-btn ml-3">
                                <button type="button" id="btnFilter" name="btnFilter" class="btn btn-primary px-3">Tra cứu</button>
                            </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->

                <div class="mb-3 alert alert-success d-none text-center" id="filterMessage">
                    Rất tiếc, bạn chưa nằm trong danh sách trúng thưởng. Chúc bạn may mắn và nhiều sức khỏe!
                </div>

                <div style="max-height: 500px; overflow-y: scroll;">
                    <table class="table table-bordered pmc-table" id="tableResult">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th class="text-nowrap">Số điện thoại</th>
                        <th class="text-nowrap">Ngày trúng</th>
                        <th class="text-nowrap">Giải thưởng</th>
                        <th class="text-nowrap">Phần thưởng</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($games as $key => $game)
                        <tr id="game_{{ $game->id }}">
                            <td class="text-center">{{ $key + 1 }}</td>
                            <td class="text-nowrap">{{ $game->phone_masked }}</td>
                            <td class="text-nowrap text-center">{{ $game->created_at }}</td>
                            <td class="text-nowrap text-center text-uppercase">{{ $game->gift_title }} - {{ $game->gift->title }}</td>
                            <td class="text-nowrap text-center">{{ $game->gift_name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section id="section-qna" class="mb-5" data-scroll-index="2">
        <div class="container">
            <h5 class="bg-success2 text-white px-3 py-2 strong mb-0 rounded">HỎI & ĐÁP</h5>
            <div class="bg-white p-3">
                <h2 class="text-center text-uppercase text-primary strong mb-3">Những câu hỏi thường gặp</h2>

                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    1. Tôi cần làm gì để tham gia chương trình “CHÀO MỪNG NHÀ THUỐC 500”
                                </a>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Để tham gia chương trình, bạn cần đáp ứng điều kiện sau:
                                <ul>
                               		<li>Có phát sinh giao dịch mua hàng tại các nhà thuốc Pharmacity hoặc mua hàng trực tuyến tại Pharmacity.com trong thời gian khuyến mãi Với bất kì hóa đơn từ 100.000đ khách hàng sẽ nhận được 01 mã số may mắn</li>
                                    <li>Bạn là thành viên Extracare  </li>

                                </ul>
                                <p>•	Sau khi thanh toán 1 đơn hàng, bạn sẽ nhận 1 mã số để tham gia vòng bốc thăm may mắn nhathuoc500.pharmacity.vn</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    2. Nếu tôi đã mua hàng tại Pharmacity nhưng chưa đăng kí ExtraCare thì có được tham gia chương trình không?
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Chương trình chỉ áp dụng cho khách hàng là thành viên ExtraCare
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    3. “Mã số” tôi có thể nhận được khi mua hàng tại Pharmacity là số gì?
                                </a>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Mỗi khách hàng khi phát sinh 01 giao dịch mua hàng tại Pharmacity và được hệ thống ghi nhận trong thời gian chương trình diễn ra sẽ nhận được 1 mã số gồm 2 chữ cái và 7 chữ số từ 0000001 đến 9999999, ví dụ: CM1234567.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading4">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    4. Tôi mua hàng trực tuyến trên website của Pharmacity, vậy có được tham gia chương trình không?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                            <div class="card-body">
                                Chương trình áp dụng cho các khách hàng mua hàng trực tuyến trên website của Pharmacity tại Pharmacity.vn
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading5">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                    5. Tôi là thành viên ExtraCare, nhưng khi mua hàng tại Pharmacity tôi lại để quên thẻ, tôi có thể tham gia chương trình không?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                            <div class="card-body">
                                Khách hàng chỉ cần đọc số điện thoại đăng kí ExtraCare và đăng nhập vào website để chơi game
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading7">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                    6. Nếu trúng thưởng, tôi phải làm gì để nhận giải?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                            <div class="card-body">
                                <ul>
                                    <li>Nhận giải tại trụ sở Công ty CP Dược phẩm Pharmacity - 248A Nơ Trang Long, Phường 12, Quận Bình Thạnh, TP.HCM. Sẽ được hẹn trao giải sau 07 ngày gọi điện thoại về tổng đài 1800 2001 kể từ ngày KH gọi điện thoại thông báo trúng thưởng.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading8">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                    7. Tôi muốn kiểm tra kết quả trúng thưởng của chương trình, tôi có thể theo dõi tại đâu
                                </a>
                            </h5>
                        </div>
                        <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                            <div class="card-body">
                                Bạn có thể truy cập trang nhathuoc500.pharmaicity.vn để kiểm tra kết quả trúng thưởng của chương trình
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>

    </section>

    <div class="modal" tabindex="-1" role="dialog" id="luckyModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Bốc thăm trúng thưởng</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="pt-3">
                        <p class="text-center text-success mb-5">Bạn hãy Click vào hộp quà may mắn bên dưới để có cơ hội nhận phần quà hấp dẫn nhé
                        </p>
                        <div class="row mb-3">
                            <div class="col"><img class="img-fluid" src="{{ asset('images/gift-small.png') }}"/></div>
                            <div class="col"><img class="img-fluid" src="{{ asset('images/gift-small.png') }}"/></div>
                            <div class="col"><img class="img-fluid" src="{{ asset('images/gift-small.png') }}"/></div>
                            <div class="col"><img class="img-fluid" src="{{ asset('images/gift-small.png') }}"/></div>
                        </div>

                        <div class="mb-0 alert alert-danger d-none text-center" id="modalErrors"></div>
                        <div class="mb-0 alert alert-success d-none text-center" id="modalMessage"></div>
                    </div>

                    <div class="text-center d-none text-primary loading">
                        <div class="mt-5 mb-3"><img src="{{ asset('images/loading.svg') }}" /> </div>
                        Hệ thống đang xử lý, vui lòng đợi trong giây lát...
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
