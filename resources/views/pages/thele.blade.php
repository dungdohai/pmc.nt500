@extends('layout-official')

@section('content')

    <section id="gameSection" class="px-md-0 text-white">
        <div class="container">
            <div class="row sm-gutters">
                <div class="col-md-9 order-md-last">
                    <div class="my-3 d-md-none text-center">
                        <img class="img-fluid w-75 mx-auto" src="{{ asset('images/official/text-game.png') }}" />
                    </div>
                    <div id="banner" class="py-1 py-md-3 pb-md-0">
                        <div class="row no-gutters">
                            <div class="col-3">
                                {{--<img class="img-fluid" src="{{ asset('images/official/banner-left.png') }}" />--}}
                            </div>
                            <div class="col-7">
                                <div class="row mt-3 mt-md-5 sm-gutters btn-glow" id="btnShowResult">
                                    <div class="col-4 pt-3 pt-md-5">
                                        <div class="p-2"><img class="img-fluid" src="{{ asset('images/official/banner-prd-01.png') }}" /></div>
                                    </div>
                                    <div class="col-4">
                                        <div class="p-2"><img class="img-fluid" src="{{ asset('images/official/banner-prd-02.png') }}" /></div>
                                </div>
                                    <div class="col-4 pt-3 pt-md-5">
                                        <div class="p-2"><img class="img-fluid" src="{{ asset('images/official/banner-prd-03.png') }}" /></div>
                            </div>
                                </div>
                            </div>
                            <div class="col-2">
                                {{--<img class="img-fluid" src="{{ asset('images/official/banner-right.png') }}" />--}}
                            </div>
                        </div>
                    </div>
                    {{--<div id="quaChau" class="text-center mb-5 d-none">--}}
                    {{--<a href="javascript:void(0);" id="btnShowResult">--}}
                    {{--<span><img src="{{ asset('images/icon-gs-single.png') }}" class="img-fluid"/></span>--}}
                    {{--<span><img src="{{ asset('images/icon-gs-single.png') }}" class="img-fluid"/></span>--}}
                    {{--<span><img src="{{ asset('images/icon-gs-single.png') }}" class="img-fluid"/></span>--}}
                    {{--</a>--}}
                    {{--</div>--}}
                </div>
                <div class="col-md-3 pt-md-4">
                    <div id="loading" class="text-center d-none text-primary">
                        <div class="mt-5 mb-3"><img src="{{ asset('images/loading.svg') }}" /> </div>
                        Hệ thống đang xử lý, vui lòng đợi trong giây lát...
                    </div>

                    <div class="strong mb-3 d-none d-md-block">
                        <img class="img-fluid" src="{{ asset('images/official/text-game.png') }}" />
                    </div>

                    <form id="gameForm" action="{{ route('api') }}" class="mt-3">
                        <div class="form-group">
                            <label class="sr-only" for="userEmail">Số điện thoại</label>
                            <input type="number" class="form-control" id="userPhone" name="user_phone" placeholder="Nhập số điện thoại ExtraCare" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="userCode">Nhập mã</label>
                            <input type="text" class="form-control" id="userCode" name="user_code" placeholder="Nhập mã trên hoá đơn" />
                        </div>
                        <div class="mb-3 alert alert-danger d-none text-center" id="formErrors"></div>
                        <div class="mb-3 alert alert-success d-none text-center" id="formMessage"></div>
                        <div class="mb-3 alert alert-info d-none text-center" id="formInfo"></div>

                        <div class="text-center mb-3 d-none" id="boxLike">
                            <div class="mb-3 text-white">Hãy Like Fanpage Pharmacity để nhận 01 lượt chơi!</div>
                            <div class="fb-like" data-href="https://www.facebook.com/PharmacityVN/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                            <div>
                                <button type="button" id="freeCode" class="btn btn-sm btn-primary w-50 mt-3 d-none">Nhận 1 lượt miễn phí</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <button id="btnSubmit" type="submit" class="btn btn-block btn-success">Nhận lượt mở quà</button>
                        </div>

                        <div class="text-center mb-3"><a href="#sectionRegister" class="sliding-link text-white">Bạn chưa có ExtraCare? Đăng ký ngay!</a> </div>
                    </form>
                </div>
            </div>

            <div id="giftList" class="row sm-gutters pt-3 pb-3">
                <div class="col-3 gift-item">
                    <div class="row no-gutters">
                        <div class="col-md-5"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-1.png') }}" alt="Giải đặc biệt"></div>
                        <div class="col-md-7 mt-4">
                            <h6 class="mt-0 text-uppercase strong text-nowrap">1 Giải đặc biệt</h6>
                            <div>iPhone XS MAX 256Gb</div>
                        </div>
                    </div>
                </div>
                <div class="col-3 gift-item mt-4">
                    <div class="row no-gutters">
                        <div class="col-md-4"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-2.png') }}" alt="Generic placeholder image"></div>
                        <div class="col-md-8">
                            <h6 class="mt-0 text-uppercase strong">5 Giải nhất</h6>
                            <div>Một chỉ vàng SJC
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 gift-item mt-4">
                    <div class="row no-gutters">
                        <div class="col-md-4"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-3.png') }}" alt="Generic placeholder image"></div>
                        <div class="col-md-8">
                            <div class="ml-3">
                                <h6 class="mt-0 text-uppercase strong">50 Giải nhì</h6>
                                <div>Một chai viên uống bổ sung Vitamin C Blackmores BIO C 1000mg</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 gift-item mt-4">
                    <div class="row no-gutters">
                        <div class="col-md-4"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-4.png') }}" alt="Generic placeholder image"></div>
                        <div class="col-md-8">
                            <div class="ml-3">
                                <h6 class="mt-0 text-uppercase strong">200 Giải ba</h6>
                                <div>Tích luỹ 20.000đ vào thẻ Extracare</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @include('partials/navi2')
    <div class="mb-3"></div>
    @foreach ($sections as $section)
        <section id="section-{{ $section->id }}" class="mb-3 mb-md-5" data-scroll-index="0">
            <div class="container">
                <h5 class="bg-success text-white px-3 py-2 strong mb-0 rounded">{{ $section->title }}</h5>
                <div class="border border-top-0 py-3 px-4 section-content bg-white">
                    {!! $section->teaser !!}

                    @if ($section->content !== null)
                        <div id="content-{{ $section->id }}" class="collapse">
                            {!! $section->content !!}
                        </div>
                        <div class="text-center">
                            <a href="#content-{{ $section->id }}" class="btn btn-outline-primary btn-toggle collapsed" data-toggle="collapse">
                                <span class="expand-text">Xem thêm</span>
                                <span class="collapse-text">Thu gọn</span>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </section>
    @endforeach

    <section id="section-winners" class="mb-3 mb-md-5" data-scroll-index="1">
        <div class="container">
            <h5 class="bg-success text-white px-3 py-2 strong mb-0 rounded">DANH SÁCH TRÚNG THƯỞNG</h5>
            <div class="border border-top-0 py-3 px-4 section-content bg-white">
                <div class="row mb-3">
                    <div class="col-lg-4 offset-lg-4">
                        <div class="input-group">
                            <input type="number" class="form-control" id="filterPhone" name="filter-phone" placeholder="Số điện thoại">
                            <span class="input-group-btn ml-3">
                                <button type="button" id="btnFilter" name="btnFilter" class="btn btn-primary px-3">Tra cứu</button>
                            </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->

                <div class="mb-3 alert alert-success d-none text-center" id="filterMessage">
                    Rất tiếc, bạn chưa nằm trong danh sách trúng thưởng. Chúc bạn may mắn và nhiều sức khỏe!
                </div>

                <div style="max-height: 500px; overflow-y: scroll;">
                    <table class="table table-bordered pmc-table" id="tableResult">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Số điện thoại</th>
                        <th>Ngày trúng</th>
                        <th>Phần thưởng</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($games as $key => $game)
                        <tr id="game_{{ $game->id }}">
                            <td class="text-center">{{ $key + 1 }}</td>
                            <td>{{ $game->phone_masked }}</td>
                            <td class="text-center">{{ $game->created_at }}</td>
                            <td class="text-center text-uppercase">{{ $game->gift_title }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section id="section-qna" class="mb-5" data-scroll-index="2">
        <div class="container">
            <h5 class="bg-success text-white px-3 py-2 strong mb-0 rounded">HỎI & ĐÁP</h5>
            <div class="bg-white p-3">
                <h2 class="text-center text-uppercase text-primary strong mb-3">Những câu hỏi thường gặp</h2>

                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    1. Tôi cần làm gì để tham gia chương trình “MUA BLACKMORES RINH IPHONE XS MAX 256GB”
                                </a>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Để tham gia chương trình, bạn cần đáp ứng 2 điều kiện sau:
                                <ul>
                                    <li>Đã có thẻ chăm sóc khách hàng thân thiết của Pharmacity (Extracare).</li>
                                    <li>Có phát sinh giao dịch mua hàng tại các nhà thuốc Pharmacity hoặc mua hàng trực tuyến tại Pharmacity.com trong thời gian khuyến mãi và sử dụng thẻ Extracare khi thanh toán. (Bạn nhớ thông báo trạng thái thành viên Extracare bằng cách đưa thẻ Extracare hoặc đọc số điện thoại đăng ký Extracare với nhân viên nhà thuốc Pharmacity trước khi thanh toán).</li>
                                </ul>
                                <p>Sau khi thanh toán 1 đơn hàng, bạn sẽ nhận 1 mã số may mắn để quy đổi thành lượt mở quà tại trang blackmores.pharmacity.vn. Hãy dùng lượt mở này để mở quà may mắn.</p>
                                <p class="strong">* Danh sách sản phẩm Blackmores áp dụng trong chương trình:</p>
                                <p>•	Blackmores Ginkgoforte (Hộp 40 viên)<br/>
                                •	Blackmores Women's Vitality Multi (Hộp 50 viên)<br/>
                                •	Blackmores Bio C 1000mg (Hộp 31 viên)<br/>
                                •	Blackmores CoQ10 150mg (Chai/30 viên)<br/>
                                •	Blackmores Pregnancy And Breast Feeding Gold (Chai 60 viên)<br/>
                                •	Blackmores Oudourless Fish Oil 1000 (Chai 200 viên)<br/>
                                •	Blackmores Evening Primrose Oil (Chai 190 viên)<br/>
                                •	Blackmores Glucosamine 1500 (Chai 90 viên)<br/>
                                    •	Blackmores Omega Daily Fish Oil 1000mg (Chai 90 viên)</p>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    2. Nếu tôi đã mua hàng tại Pharmacity nhưng chưa đăng ký Extracare thì có được tham gia chương trình không?
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Chương trình chỉ áp dụng cho thành viên Extracare, vì vậy bạn chưa thể tham gia chương trình. Bạn có thể đăng ký Extracare trực tiếp tại nhà thuốc trước khi thanh toán để được tham gia, hãy liên hệ nhân viên nhà thuốc để được hỗ trợ chi tiết.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    3. “Mã số” tôi có thể nhận được khi mua hàng tại Pharmacity là số gì?
                                </a>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Mỗi thành viên Extracare khi phát sinh 01 giao dịch mua hàng tại Pharmacity và được hệ thống ghi nhận (bạn hãy thông báo trạng thái thành viên Extracare cho nhân viên nhà thuốc trước khi thanh toán) trong thời gian chương trình diễn ra sẽ nhận được 1 mã số gồm 2 chữ cái và 6 chữ số từ 000001 đến 999999, ví dụ: BM123456. Khách hành truy cập vào blackmores.pharmacity.vn để nhập số điện thoại đăng ký Extracare và mã số để nhận lượt mở quà. 1 mã số = 1 lượt mở.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading4">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    4. Tôi mua hàng trực tuyến trên website của Pharmacity, vậy có được tham gia chương trình không?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                            <div class="card-body">
                                Chương trình áp dụng cho các thành viên Extracare mua hàng trực tuyến trên website của Pharmacity tại Pharmacity.vn hoặc Pharmacity.com. Bạn hãy đăng nhập tài khoản Extracare trước khi thực hiện giao dịch và thanh toán.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading5">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                    5. Tôi là thành viên Extracare nhưng khi mua hàng tại Pharmacity tôi để quên thẻ, tôi có thể tham gia chương trình không?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                            <div class="card-body">
                                Thành viên Extracare có thể đọc số điện thoại đã đăng ký thẻ thành viên Extracare khi thanh toán để nhận mã số may mắn, sau đó truy cập vào trang blackmores.pharmacity.vn và nhập số điện thoại cùng với mã số may mắn để nhận lượt mở quà.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading6">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                    6. Tôi là thành viên Extracare nhưng khi mua hàng tại Pharmacity tôi không dùng thẻ để tích điểm và thanh toán, tôi có nhận được tham gia chương trình không?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                            <div class="card-body">
                                Rất tiếc, nếu bạn không dùng thẻ Extracare hoặc không thông báo số điện thoại đã đăng ký Extracare cho nhân viên nhà thuốc để tích điểm và thanh toán, hệ thống sẽ không có thông tin để ghi nhận lượt thanh toán của bạn và sẽ không thể đưa số điện thoại đăng ký Extracare của bạn vào danh sách tham gia chương trình.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading7">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                    7. Nếu trúng thưởng, tôi phải làm gì để nhận giải?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                            <div class="card-body">
                                Giải thưởng sẽ được trao cho khách hàng trong vòng 07 ngày kể từ ngày khách hàng gọi điện về tổng đài 1800 6821 thông báo trúng giải.
                                <ul>
                                    <li>Khách hàng trúng giải Đặc biệt, giải Nhất và giải Nhì: Nhận giải tại trụ sở Công ty CP Dược phẩm Pharmacity - 248A Nơ Trang Long, Phường 12, Quận Bình Thạnh, TP.HCM. Sẽ được hẹn trao giải sau 07 ngày gọi điện thoại về tổng đài 1800 6821 kể từ ngày KH gọi điện thoại thông báo trúng thưởng.
                                    </li>
                                    <li>Đối với khách hàng trúng Giải Ba – 20.000 Điểm tích lũy Extracare, khách hàng sẽ được tích lũy trực tiếp vào thẻ chăm sóc khách hàng thân thiết (Extracare) trong vòng 10 ngày.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading8">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                    8. Tôi quên số điện thoại đăng ký thành viên Extracare, tôi phải làm gì để được tham gia chương trình?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
                            <div class="card-body">
                                Để được hỗ trợ và hướng dẫn chi tiết lấy lại thông tin thành viên Extracare, bạn vui lòng liên hệ hotline (miễn phí cuộc gọi) 1800 6821.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading9">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                    9. Tôi vừa mua hàng tại Pharmacity nhưng không có mã số dự thưởng?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
                            <div class="card-body">
                                Trước khi bạn thanh toán mua hàng tại các nhà thuốc Pharmacity, hãy thông báo với nhân viên nhà thuốc số điện thoại đăng ký Extracare hoặc thẻ thành viên Extracare. Khi xuất hóa đơn, mã số của bạn sẽ nằm trên hóa đơn.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading10">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                    10. Tôi nhập mã số may mắn, nhưng không mở quà may mắn được
                                </a>
                            </h5>
                        </div>
                        <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
                            <div class="card-body">
                                Để đảm bảo hệ thống đã được đồng bộ. Bạn vui lòng chờ trong 15 phút sau khi mua hàng để có thể tham gia mở quà may mắn.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading11">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                    11. Tôi muốn kiểm tra kết quả trúng thưởng của chương trình, tôi có thể theo dõi tại đâu?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                            <div class="card-body">
                                Bạn có thể truy cập trang blackmores.pharmacity.vn để kiểm tra kết quả trúng thưởng của chương trình.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection