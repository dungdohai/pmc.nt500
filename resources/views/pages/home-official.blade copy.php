@extends('layout-official')

@section('content')

    <section id="gameSection" class="px-md-0 text-white">
        <div class="container">
            <div class="row sm-gutters">
                <div class="col-md-9 order-md-last">
                    <div class="my-3 d-md-none text-center">
                        <img class="img-fluid w-75 mx-auto" src="{{ asset('images/official/text-game.png') }}" />
                    </div>
                    <div id="banner" class="py-1 py-md-3 pb-md-0">
                        <div class="row no-gutters">
                            <div class="col-3">
                                {{--<img class="img-fluid" src="{{ asset('images/official/banner-left.png') }}" />--}}
                            </div>
                            <div class="col-7">
                                <div class="row mt-3 mt-md-5 sm-gutters btn-glow" id="btnShowResult">
                                    <div class="col-4 pt-3 pt-md-5">
                                        <div class="p-2"><img class="img-fluid" src="{{ asset('images/official/banner-prd-01.png') }}" /></div>
                                    </div>
                                    <div class="col-4">
                                        <div class="p-2"><img class="img-fluid" src="{{ asset('images/official/banner-prd-02.png') }}" /></div>
                                </div>
                                    <div class="col-4 pt-3 pt-md-5">
                                        <div class="p-2"><img class="img-fluid" src="{{ asset('images/official/banner-prd-03.png') }}" /></div>
                            </div>
                                </div>
                            </div>
                            <div class="col-2">
                                {{--<img class="img-fluid" src="{{ asset('images/official/banner-right.png') }}" />--}}
                            </div>
                        </div>
                    </div>
                    {{--<div id="quaChau" class="text-center mb-5 d-none">--}}
                    {{--<a href="javascript:void(0);" id="btnShowResult">--}}
                    {{--<span><img src="{{ asset('images/icon-gs-single.png') }}" class="img-fluid"/></span>--}}
                    {{--<span><img src="{{ asset('images/icon-gs-single.png') }}" class="img-fluid"/></span>--}}
                    {{--<span><img src="{{ asset('images/icon-gs-single.png') }}" class="img-fluid"/></span>--}}
                    {{--</a>--}}
                    {{--</div>--}}
                </div>
                <div class="col-md-3 pt-md-4">
                    <div id="loading" class="text-center d-none text-primary">
                        <div class="mt-5 mb-3"><img src="{{ asset('images/loading.svg') }}" /> </div>
                        Hệ thống đang xử lý, vui lòng đợi trong giây lát...
                    </div>

                    <div class="strong mb-3 d-none d-md-block">
                        <img class="img-fluid" src="{{ asset('images/official/text-game.png') }}" />
                    </div>

                    <form id="gameForm" action="{{ route('api') }}" class="mt-3">
                        <div class="form-group">
                            <label class="sr-only" for="userEmail">Số điện thoại</label>
                            <input type="number" class="form-control" id="userPhone" name="user_phone" placeholder="Nhập số điện thoại ExtraCare" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="userCode">Nhập mã</label>
                            <input type="text" class="form-control" id="userCode" name="user_code" placeholder="Nhập mã trên hoá đơn" />
                        </div>
                        <div class="mb-3 alert alert-danger d-none text-center" id="formErrors"></div>
                        <div class="mb-3 alert alert-success d-none text-center" id="formMessage"></div>
                        <div class="mb-3 alert alert-info d-none text-center" id="formInfo"></div>

                        <div class="text-center mb-3 d-none" id="boxLike">
                            <div class="mb-3 text-white">Hãy Like Fanpage Pharmacity để nhận 01 lượt chơi!</div>
                            <div class="fb-like" data-href="https://www.facebook.com/PharmacityVN/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                            <div>
                                <button type="button" id="freeCode" class="btn btn-sm btn-primary w-50 mt-3 d-none">Nhận 1 lượt miễn phí</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <button id="btnSubmit" type="submit" class="btn btn-block btn-success">Nhận lượt mở quà</button>
                        </div>

                        <div class="text-center mb-3"><a href="#sectionRegister" class="sliding-link text-white">Bạn chưa có ExtraCare? Đăng ký ngay!</a> </div>
                    </form>
                </div>
            </div>

            <div id="giftList" class="row sm-gutters pt-3 pb-3">
                <div class="col-3 gift-item">
                    <div class="row no-gutters">
                        <div class="col-md-5"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-1.png') }}" alt="Giải đặc biệt"></div>
                        <div class="col-md-7 mt-4">
                            <h6 class="mt-0 text-uppercase strong text-nowrap">1 Giải đặc biệt</h6>
                            <div>iPhone XS MAX 256Gb</div>
                        </div>
                    </div>
                </div>
                <div class="col-3 gift-item mt-4">
                    <div class="row no-gutters">
                        <div class="col-md-4"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-2.png') }}" alt="Generic placeholder image"></div>
                        <div class="col-md-8">
                            <h6 class="mt-0 text-uppercase strong">5 Giải nhất</h6>
                            <div>Một chỉ vàng SJC
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 gift-item mt-4">
                    <div class="row no-gutters">
                        <div class="col-md-4"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-3.png') }}" alt="Generic placeholder image"></div>
                        <div class="col-md-8">
                            <div class="ml-3">
                                <h6 class="mt-0 text-uppercase strong">50 Giải nhì</h6>
                                <div>Một chai viên uống bổ sung Vitamin C Blackmores BIO C 1000mg</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 gift-item mt-4">
                    <div class="row no-gutters">
                        <div class="col-md-4"><img class="img-fluid mb-3" src="{{ asset('images/official/gift-4.png') }}" alt="Generic placeholder image"></div>
                        <div class="col-md-8">
                            <div class="ml-3">
                                <h6 class="mt-0 text-uppercase strong">200 Giải ba</h6>
                                <div>Tích luỹ 20.000đ vào thẻ Extracare</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @include('partials/navi')

    <section id="videoSection" class="py-5" data-scroll-index="0">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h3 class="bg-success text-center text-white py-3 strong">Mua Blackmores đến Pharmacity</h3>
                    <div class="video-container">
                        <iframe width="720" height="405" src="https://www.youtube.com/embed/tFGQCQXTd60" frameborder="0" allowfullscreen></iframe>
           
                    </div>

                    <div class="text-center mt-3">
                        <a href="#" class="my-3 share-fb" data-href="https://www.youtube.com/watch?v=tFGQCQXTd60"><img src="{{ asset('images/btn-share-fb.png') }}" /></a>
                        <a href="javascript:void(0);" class="zalo-share-button" data-href="https://www.youtube.com/watch?v=tFGQCQXTd60" data-oaid="4353069344892820555" data-customize=true><img src="{{ asset('images/btn-share-zalo.png') }}" /></a>
                    </div>


                    <div class="text-center" style="overflow: hidden;">
                        <div class="fb-like" data-href="https://www.facebook.com/PharmacityVN/" data-layout="standard" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('partials.register')

    <div id="sectionIntro" data-scroll-index="2">
        <div class="container">
            <div class="my-5 bg-white rounded">
                <h3 class="bg-success text-center text-white py-3 strong">Ưu đãi đặc biệt</h3>
                <div class="px-3">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <ul class="nav nav-tabs row" id="prmNav" role="tablist">
                                <li class="nav-item col-md-4 offset-md-2">
                                    <a class="nav-link rounded active border-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Mua hăng say<br/>quà liền tay</a>
                                </li>
                                <li class="nav-item col-md-4">
                                    <a class="nav-link rounded border-0" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Nhân đôi điểm<br/>EXTRACARE</a>
                                </li>
                                {{--<li class="nav-item col-md-4">--}}
                                    {{--<a class="nav-link rounded border-0" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Gặp Poom & Blackmores<br/>nhận quà</a>--}}
                                {{--</li>--}}
                            </ul>

                            <div class="tab-content py-5" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-md-5 text-center">
                                            <img class="img-fluid" src="{{ asset('images/official/prm-01.png') }}" />
                                        </div>
                                        <div class="col-md-7">
                                            <p>Nhận ngay một chai viên uống dầu cá <strong>Blackmores Oudourless Fish Oil 1000</strong> (Chai 200 viên) trị giá gần 500.000đ cho 15 khách hàng <strong>Extracare</strong> có doanh số mua sản phẩm <strong>Blackmores</strong> cao nhất tại <strong>Pharmacity</strong>. (mỗi khách hàng Extracare/1 chai).</p>
                                            <p><strong>Thời gian:</strong> 25/3/2019 – 25/4/2019.</p>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-md-5 text-center">
                                            <img class="img-fluid" src="{{ asset('images/official/prm-02.png') }}" />
                                        </div>
                                        <div class="col-md-7">
                                            <p>Nhân đôi điểm tích lũy vào thẻ Extracare khi mua sản phẩm Blackmores tại Pharmacity.</p>
                                            <p><strong>Thời gian:</strong> 25/3/2019 – 25/4/2019.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="row">
                                        <div class="col-md-5 text-center">
                                            <img class="img-fluid" src="{{ asset('images/official/prm-03.png') }}" />
                                        </div>
                                        <div class="col-md-7">
                                            <p>Đến gặp Poom tại các địa điểm hoạt náo sau để có cơ hội nhận quà:</p>
                                            <ul>
                                                <li><strong>Địa điểm:</strong> đang cập nhật</li>
                                                <li><strong>Thời gian:</strong> đang cập nhật</li>
                                                <li><strong>Quà tặng:</strong> 100 Chai thuốc Blackmores Bio C</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="sectionProduct" data-scroll-index="3">
        <div class="container">
            <div class="my-5 bg-white rounded">
                <h3 class="bg-success text-center text-white py-3 strong">Sản phẩm nổi bật</h3>
                <div class="px-3">
                    <div class="row prd-list">
                        @foreach ($products as $product)
                            @include('partials.prd-item')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- An Blog Suc Khoe 
    <section id="sectionBlog" data-scroll-index="4">
        <div class="container">
            <div class="my-5 bg-white rounded">
                <h3 class="bg-success text-center text-white py-3 strong">Thông tin sức khoẻ</h3>
                <div class="p-3">
                    <div class="row">
                        @foreach ($blogs as $blog)
                            <div class="col-6 col-md-3 mb-3">
                                <div>
                                    <a href="{{ $blog->link }}" target="_blank"><img class="w-100" src="{{ Storage::url($blog->image) }}"></a>
                                </div>

                                <h6 class="mt-3"><a class="text-black" target="_blank" href="{{ $blog->link }}">{{ $blog->title }}</a> </h6>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
 End of blog suc khoe --->
@endsection